package com.example.nutbourneinventorymanagerapp.FirebaseStorage;

//Class for uploading images when adding a new item/asset.
//Thanks to Coding in Flow

public class UploadImage {
    private String mName; //I'll set the image name to be the same as the item name
    private String mImageUrl;

    public UploadImage() {
        //EMPTY CONSTRUCTOR NEEDED DO NOT DELETE
    }

    public UploadImage(String name, String imageUrl) {
        if (name.trim().equals("")) { //if no name is given, but I already have validation, but just in case
            name = "No name";
        }
        mName = name;
        mImageUrl = imageUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }
}