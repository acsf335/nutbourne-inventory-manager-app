package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

import java.util.Calendar;

public class RepairDamagedItemDialog extends AppCompatDialogFragment implements DatePickerDialog.OnDateSetListener {
    private RepairDamagedItemDialogListener listener;

    private EditText description;
    private TextView selectedRepairDate;
    private Button selectDateBtn;

    private String desc, repairDate;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.repair_damaged_item_dialog, null);

        description = view.findViewById(R.id.repairInfoDesc);
        selectedRepairDate = view.findViewById(R.id.repairInfoSelectedDate);
        selectDateBtn = view.findViewById(R.id.selectRepairedDateBtn);
        selectDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        builder.setView(view).setTitle("Report item as repaired").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
            }
        }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                desc = description.getText().toString();
                repairDate = selectedRepairDate.getText().toString();
                listener.repairDamagedItem(desc, repairDate);
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (RepairDamagedItemDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement RepairDamagedItemDialogListener!!");
        }
    }

    //Date stuff thanks to CodingWithMitch
    //method for starting the date picker
    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + (month + 1) + "/" + year;
        selectedRepairDate.setText(date);
    }

    public interface RepairDamagedItemDialogListener {
        void repairDamagedItem(String description, String date); //done in ViewDamagedItemActivity
    }
}