package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;

public class StockResponsePositiveDialog extends AppCompatDialogFragment implements DatePickerDialog.OnDateSetListener {
    private StockResponsePositiveDialogListener listener;
    private TextView textViewStatus, textViewEtaDate;
    private EditText editTextAddInfo;
    private Button selectEtaBtn;
    private String updatedStatus, etaDate, additionalInfo;
    private FirebaseUser currentUserInfo = FirebaseAuth.getInstance().getCurrentUser();
    private String currentUserId, currentUserRole;
    private CollectionReference usersRef = FirebaseFirestore.getInstance().collection("Users");


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.stock_response_positive_dialog, null);

        textViewStatus = view.findViewById(R.id.statusChange);
        textViewEtaDate = view.findViewById(R.id.etaDate);
        editTextAddInfo = view.findViewById(R.id.additionalInfo);
        selectEtaBtn = view.findViewById(R.id.fulfilmentEtaBtn);
        selectEtaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        currentUserId = currentUserInfo.getUid();
        DocumentReference currentUserRef = usersRef.document(currentUserId);
        currentUserRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        currentUserRole = document.getString("role");
                        //Change the status to either approved or accepted depending on if manager or purchaser or admin
                        if (currentUserRole.equals("Manager") || currentUserRole.equals("Administrator")) {
                            updatedStatus = "Approved";
                            textViewEtaDate.setVisibility(View.GONE);
                            selectEtaBtn.setVisibility(View.GONE);
                        } else {
                            if (currentUserRole.equals("Purchaser")) {
                                updatedStatus = "Accepted";
                            }
                        }
                        textViewStatus.setText(updatedStatus);
                        textViewStatus.setTextColor(Color.GREEN);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });

        builder.setView(view).setTitle("Proceed with this request").setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //LEAVE EMPTY
                    }
                }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() { //what happens when we click confirm
            @Override
            public void onClick(DialogInterface dialog, int which) {
                additionalInfo = editTextAddInfo.getText().toString();
                etaDate = textViewEtaDate.getText().toString();
                listener.applyRequestUpdate(updatedStatus, etaDate, additionalInfo);
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (StockResponsePositiveDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement StockResponsePositiveDialogListener!!");
        }
    }

    //Date stuff thanks to CodingWithMitch
    //method for starting the date picker
    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    //gives you the date selected in the dialog
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + (month + 1) + "/" + year;
        textViewEtaDate.setText(date);
    }

    //interface for the action to do when clicking confirm. This is done in ViewStockRequestActivity class
    public interface StockResponsePositiveDialogListener {
        void applyRequestUpdate(String status, String etaDate, String additionalInfo);
    }
}