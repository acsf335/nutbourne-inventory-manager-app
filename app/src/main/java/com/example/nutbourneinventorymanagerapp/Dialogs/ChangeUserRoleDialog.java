package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

public class ChangeUserRoleDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener {

    private Spinner selectUserRole;
    private String selectedRole;
    private ChangeUserRoleDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.change_role_dialog, null);

        //Spinner setup
        selectUserRole = view.findViewById(R.id.spinnerEditUserRole);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(getActivity(), R.array.userRoles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectUserRole.setAdapter(adapter); //set the adapter
        selectUserRole.setOnItemSelectedListener(this);

        builder.setView(view).setTitle("Change User Role").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //leave empty
            }
        }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String updateUserRole = selectedRole; //get the role value selected
                listener.applyRoleChange(updateUserRole); //pass the variable into the activity here
            }
        });
        return builder.create();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedRole = parent.getItemAtPosition(position).toString(); //set the selected option to this variable
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //LEAVE EMPTY
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //initialise the listener
        try {
            listener = (ChangeUserRoleDialogListener) context; //initialise the listener
        } catch (ClassCastException e) { //if we open the dialog from UserViewActivity..
            throw new ClassCastException(context.toString() + // ..but forget to implement the listener to the activity, we get this exception.
                    " must implement ChangeUserRoleDialogListener!");
        }
    }

    //interface for the action to do when clicking confirm. This is done in UserViewActivity class
    public interface ChangeUserRoleDialogListener {
        void applyRoleChange(String updateUserRole);
    }
}