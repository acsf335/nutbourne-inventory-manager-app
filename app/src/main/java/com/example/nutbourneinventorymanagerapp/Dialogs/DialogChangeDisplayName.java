package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

//Got help for this from Coding in Flow.
//layout_dialog.xml consists of an EditText for display name

public class DialogChangeDisplayName extends AppCompatDialogFragment {

    private EditText editDisplayName;
    private DialogChangeDisplayNameListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        builder.setView(view).setTitle("Change display name")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Can be empty as this just dismisses the dialog
                    }
                })
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String displayName = editDisplayName.getText().toString(); //gets the text from the EditText
                        listener.applyChanges(displayName); //pass the edit text value
                    }
                });
        editDisplayName = view.findViewById(R.id.dialogEditDisplayName);

        //To get the keyboard to pop up automatically upon opening dialog box.
        Dialog dialog = builder.create();
        editDisplayName.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DialogChangeDisplayNameListener) context; //initialise the listener
        } catch (ClassCastException e) { //if we open the dialog from ManageAccountFragment..
            throw new ClassCastException(context.toString() + // ..but forget to implement the listener to the fragment, we get this exception.
                    "must implement DialogChangeDisplayNameListener!");
        }
    }

    public interface DialogChangeDisplayNameListener { //to pass the edit text value to the manage account fragment
        void applyChanges(String displayName); //this is done in MainActivity
    }
}