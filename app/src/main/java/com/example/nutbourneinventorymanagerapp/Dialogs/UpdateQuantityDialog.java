package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

public class UpdateQuantityDialog extends AppCompatDialogFragment {

    private NumberPicker addQuantity;
    private TextView updatedQuantity;
    private EditText addQuantityInfo;
    private Double currentQuantity; //the current quantity before updating
    private UpdateQuantityDialogListener listener;

    public void UpdateQuantityDialog(Double currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.item_add_quantity_dialog, null);

        addQuantity = view.findViewById(R.id.numPickAddQuantity);
        addQuantity.setMinValue(0);
        addQuantity.setMaxValue(1000);

        updatedQuantity = view.findViewById(R.id.updatedQuantityValue);
        addQuantityInfo = view.findViewById(R.id.addQuantityInfo);

        builder.setView(view).setTitle("Add to current Quantity").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //LEAVE EMPTY
            }
        }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int updatedQuantityValue = addQuantity.getValue();
                String updatedQuantityInfo = addQuantityInfo.getText().toString();
                listener.updateQuantity(updatedQuantityValue, updatedQuantityInfo);
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (UpdateQuantityDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement UpdateQuantityDialogListener!!");
        }

    }

    public interface UpdateQuantityDialogListener { //done in ItemViewActivity
        void updateQuantity(int updatedQuantityValue, String updatedQuantityInfo);
    }
}