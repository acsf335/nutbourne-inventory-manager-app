package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

public class ReportDamagedItemDialog extends AppCompatDialogFragment {

    private EditText description;
    private NumberPicker qty;
    private ReportDamagedItemDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.report_damaged_item_dialog, null);

        description = view.findViewById(R.id.editTextDamagedDesc);
        qty = view.findViewById(R.id.pickDamagedItemQty);
        qty.setMinValue(1);
        qty.setMaxValue(100);

        builder.setView(view).setTitle("Report this item damaged")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //LEAVE EMPTY OR ADD A TOAST. UP TO YOU
                    }
                }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String damagedDesc = description.getText().toString();
                int quantity = qty.getValue();
                listener.createDamagedItem(damagedDesc, quantity);
            }
        });

        //getting the keyboard to start on the description field
        Dialog dialog = builder.create();
        description.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (ReportDamagedItemDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement ReportDamagedItemDialogListener!!");
        }
    }

    public interface ReportDamagedItemDialogListener {
        void createDamagedItem(String damagedDescription, int quantity); //done in ItemViewActivity
    }
}