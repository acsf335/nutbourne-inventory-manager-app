package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;

public class CreateCategoryDialog extends AppCompatDialogFragment {

    private EditText createCategoryName;
    private CreateCategoryDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.create_category_dialog, null);

        createCategoryName = view.findViewById(R.id.createCategoryName);

        builder.setView(view).setTitle("Create new category")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //LEAVE EMPTY
                    }
                })
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String categoryName = createCategoryName.getText().toString();
                        listener.createCategory(categoryName);
                    }
                });
        //To get the keyboard to pop up automatically upon opening dialog box.
        Dialog dialog = builder.create();
        createCategoryName.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (CreateCategoryDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement CreateCategoryDialogListener!!");
        }
    }

    public interface CreateCategoryDialogListener {
        void createCategory(String categoryName); //done in MainActivity
    }
}