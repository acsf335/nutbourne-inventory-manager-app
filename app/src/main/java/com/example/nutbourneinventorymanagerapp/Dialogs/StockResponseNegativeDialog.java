package com.example.nutbourneinventorymanagerapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

//for rejected or denying the stock request
public class StockResponseNegativeDialog extends AppCompatDialogFragment {
    private StockResponseNegativeDialogListener listener;

    private TextView textViewStatus;
    private EditText editTextReason;

    private String updatedStatus, rejectReason;

    private FirebaseUser currentUserInfo = FirebaseAuth.getInstance().getCurrentUser();
    private String currentUserId, currentUserRole;
    private CollectionReference usersRef = FirebaseFirestore.getInstance().collection("Users");

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.stock_response_negative_dialog, null);

        textViewStatus = view.findViewById(R.id.negStatusChange);
        editTextReason = view.findViewById(R.id.negReason);

        currentUserId = currentUserInfo.getUid();
        DocumentReference currentUserRef = usersRef.document(currentUserId);
        currentUserRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        currentUserRole = document.getString("role");
                        //Change the status to either approved or accepted depending on if manager or purchaser or admin
                        if (currentUserRole.equals("Manager") || currentUserRole.equals("Administrator")) {
                            updatedStatus = "Denied";
                        } else {
                            if (currentUserRole.equals("Purchaser")) {
                                updatedStatus = "Rejected";
                            }
                        }
                        textViewStatus.setText(updatedStatus);
                        textViewStatus.setTextColor(Color.RED);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });

        builder.setView(view).setTitle("Proceed with this request").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //LEAVE EMPTY
            }
        }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() { //what happens when we click confirm
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rejectReason = editTextReason.getText().toString();
                listener.applyRequestNegativeUpdate(updatedStatus, rejectReason);
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (StockResponseNegativeDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement StockResponseNegativeDialogListener!!");
        }
    }

    //interface for the action to do when clicking confirm. This is done in ViewStockRequestActivity class
    public interface StockResponseNegativeDialogListener {
        void applyRequestNegativeUpdate(String status, String reason);
    }
}