package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

public class DamagedItem {

    private String itemId, itemName, status, description, dateReported, dateRepaired, userIdReported, userNameReported, repairInfo;
    private int quantity;

    public DamagedItem() {
        //EMPTY CONSRUCTOR
    }

    public DamagedItem(String itemId, String itemName, String status, String description, String dateReported,
                       String dateRepaired, int quantity, String userIdReported, String userNameReported, String repairInfo) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.status = status;
        this.description = description;
        this.dateRepaired = dateRepaired;
        this.dateReported = dateReported;
        this.quantity = quantity;
        this.userIdReported = userIdReported;
        this.userNameReported = userNameReported;
        this.repairInfo = repairInfo;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getDateReported() {
        return dateReported;
    }

    public String getDateRepaired() {
        return dateRepaired;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getUserIdReported() {
        return userIdReported;
    }

    public String getUserNameReported() {
        return userNameReported;
    }

    public String getRepairInfo() {
        return repairInfo;
    }
}