package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

//The java class for the layout file

public class AssetFolders {
    private String category;

    private AssetFolders() {
        //EMPTY CONSTRUCTOR NEEDED
    }

    public AssetFolders(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}