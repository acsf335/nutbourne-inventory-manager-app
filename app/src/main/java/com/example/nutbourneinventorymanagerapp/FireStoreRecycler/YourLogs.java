package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

import com.google.firebase.Timestamp;

import java.util.Date;

public class YourLogs {
    private Date timestamp;
    private String logID, userID, userName, itemName, logType;
    private int quantity;

    private YourLogs() {
        //EMPTY CONSTRUCTOR NEEDED
    }

    public YourLogs(String logID, String userID, String userName, Timestamp timestamp, String itemName,
                    int quantity, String logType) {
        this.logID = logID;
        this.userID = userID;
        this.userName = userName;
        this.timestamp = timestamp.toDate();
        this.itemName = itemName;
        this.quantity = quantity;
        this.logType = logType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getLogID() {
        return logID;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getItemName() {
        return itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getLogType() {
        return logType;
    }
}