package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

//Java class for the manage_users_recycler.xml layout

public class ManageUsersList {
    //Variables of the fields in the FB firestore document
    private String displayName;
    private String emailAddress;
    private String role;

    private ManageUsersList() {
        //empty constructor needed
    }

    //constructor
    public ManageUsersList(String username, String email, String role) {
        this.displayName = username;
        this.emailAddress = email;
        this.role = role;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getRole() {
        return role;
    }
}