package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.PropertyName;

import java.util.Date;

public class ItemQuantityLog {

    private int original_quantity;
    private int added_quantity;
    private String information;
    private Date timestamp;
    private String userStamp;

    public ItemQuantityLog() {
        //EMPTY CONSTRUCTOR NEEDED
    }

    public ItemQuantityLog(int original_quantity, int added_quantity, String information, Timestamp timestamp, String userStamp) {
        this.original_quantity = original_quantity;
        this.added_quantity = added_quantity;
        this.information = information;
        this.timestamp = timestamp.toDate();
        this.userStamp = userStamp;
    }

    @PropertyName("original_quantity")
    public int getOriginalQuantity() {
        return original_quantity;
    }

    @PropertyName("added_quantity")
    public int getAddedQuantity() {
        return added_quantity;
    }

    public String getInformation() {
        return information;
    }

    public Date getTimeStamp() {
        return timestamp;
    }

    public String getUserStamp() {
        return userStamp;
    }
}