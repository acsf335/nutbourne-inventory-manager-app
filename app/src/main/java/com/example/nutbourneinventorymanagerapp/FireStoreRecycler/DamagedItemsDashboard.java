package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;
//this is for Damaged items page from the dashboard, not from ViewDamagedItemActivity and stuff within the item
public class DamagedItemsDashboard {
    private String damagedItemID, dateReported, itemID, itemName, status, userName;
    private int quantity;

    public DamagedItemsDashboard() {
        //EMPTY CONSTRUCTOR
    }

    public DamagedItemsDashboard(String damagedItemID, String dateReported, String itemID, String itemName,
                                 String status, String userName, int quantity) {
        this.damagedItemID = damagedItemID;
        this.dateReported = dateReported;
        this.itemID = itemID;
        this.itemName = itemName;
        this.status = status;
        this.userName = userName;
        this.quantity = quantity;
    }

    public String getDamagedItemID() {
        return damagedItemID;
    }

    public String getDateReported() {
        return dateReported;
    }

    public String getItemID() {
        return itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public String getStatus() {
        return status;
    }

    public String getUserName() {
        return userName;
    }

    public int getQuantity() {
        return quantity;
    }
}