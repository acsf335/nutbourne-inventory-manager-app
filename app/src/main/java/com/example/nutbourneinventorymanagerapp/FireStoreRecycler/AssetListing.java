package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;
//The Java Class for the asset_listing_recycler.xml layout file to make the listing/cards work. Not a recycler
//Got help from Coding in flow

public class AssetListing {
    private String title;
    private String description;
    private String price;
    private String assetTag;
    private String assignedUser;
    private String imageData;

    private AssetListing() {
        //empty constructor needed as FB needs it to create the objects in the Documents DB
    }

    //constructor
    public AssetListing(String title, String description, String price, String assetTag, String assignedUser, String imageData) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.assetTag = assetTag;
        this.assignedUser = assignedUser;
        this.imageData = imageData;
    }

    //getters to get the values
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getAssetTag() {
        return assetTag;
    }

    public String getAssignedUser() {
        return assignedUser;
    }

    public String getImageData() {
        return imageData;
    }
}