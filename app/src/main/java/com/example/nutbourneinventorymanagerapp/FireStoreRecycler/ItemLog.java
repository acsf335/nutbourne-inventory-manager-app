package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

import com.google.firebase.Timestamp;

import java.util.Date;

public class ItemLog {
    private String itemName;
    private int quantity;
    private String logType;
    private String returnDate;
    private String ticketNumber;
    private String description;
    private String userID;
    private String userName;
    private Date timestamp;

    private ItemLog() {
        //LEAVE EMPTY
    }

    //Constructor
    public ItemLog(String itemName, int quantity, String logType, String returnDate, String ticketNumber, String description,
                   String userID, String userName, Timestamp timestamp) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.logType = logType;
        this.returnDate = returnDate;
        this.ticketNumber = ticketNumber;
        this.description = description;
        this.userID = userID;
        this.userName = userName;
        this.timestamp = timestamp.toDate();
    }

    public String getItemName() {
        return itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getLogType() {
        return logType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}