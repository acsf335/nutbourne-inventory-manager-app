package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;
//The Java Class for the item_listing_recycler.xml layout file to make the listing/cards work. Not a recycler
//Got help from Coding in flow

import android.media.Image;

public class ItemListing {
    //Variables of all the fields that go into the FB document
    private String title;
    private String description;
    private int quantity;
    private String price;
    private String imageData;
    private Image itemImage;

    private ItemListing() {
        //empty constructor needed as FB needs it to create the objects in the Documents DB
    }

    //constructor
    public ItemListing(String title, String description, int quantity, String price, String imageData) {
        this.title = title;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.imageData = imageData;
    }

    //getters to get the values
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

    public String getImageData() {
        return imageData;
    }
}