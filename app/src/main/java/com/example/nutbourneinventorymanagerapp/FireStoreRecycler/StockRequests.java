package com.example.nutbourneinventorymanagerapp.FireStoreRecycler;

import com.google.firebase.Timestamp;

import java.util.Date;

public class StockRequests {

    private String userId, userName, itemId, itemName, reasonReq, status, dateRequired,
            managerName, purchaserName, etaDate;
    private int quantityReq;
    private Date timestamp;

    public StockRequests() {
        //EMPTY CONSTRUCTOR NEEDED
    }

    public StockRequests(String userId, String userName, String itemId, String itemName, int quantityReq, String dateRequired,
                         Timestamp timestamp, String reasonReq, String status, String managerName, String purchaserName, String etaDate) {
        this.userId = userId;
        this.userName = userName;
        this.itemId = itemId;
        this.itemName = itemName;
        this.quantityReq = quantityReq;
        this.dateRequired = dateRequired;
        this.timestamp = timestamp.toDate();
        this.reasonReq = reasonReq;
        this.status = status;
        this.managerName = managerName;
        this.purchaserName = purchaserName;
        this.etaDate = etaDate;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public int getQuantityReq() {
        return quantityReq;
    }

    public String getDateRequired() {
        return dateRequired;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getReasonReq() {
        return reasonReq;
    }

    public String getStatus() {
        return status;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getPurchaserName() {
        return purchaserName;
    }

    public String getEtaDate() {
        return etaDate;
    }
}