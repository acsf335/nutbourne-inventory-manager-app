package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

//I got help for the login page from the Simplified Coding Firebase Authentication tutorials.
//LoginActivity is the launcher only if logged out, but if logged in already, then it goes to MainActivity instead.

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextEmail, editTextPassword;
    ProgressBar progressBar;
    private FirebaseAuth mAuth; //declare instance of this

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance(); //initialises the instance

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        progressBar = findViewById(R.id.progressBar);

        //attach listeners to the buttons
        findViewById(R.id.login_button).setOnClickListener(this); //attach listener to the login button via code instead of onClick
        findViewById(R.id.goToSignUp_button).setOnClickListener(this);
        findViewById(R.id.forgotPasswordButton).setOnClickListener(this);
    }

    private void userLogin() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        //Validation for the text entries
        if (email.isEmpty()) {
            editTextEmail.setError("Email is required!");
            editTextEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) { //if format is not email
            editTextEmail.setError("Please enter a valid email");
            editTextEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Password is required!");
            editTextPassword.requestFocus();
            return;
        }
        if (password.length() < 6) { //FB min pwd length is 6
            editTextPassword.setError("Minimum Password Length is 6.");
            editTextPassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE); //set progress bar to visible when logging in after pressing successful login button

        //if it goes past all these if statements, then it is successful login, so then do this stuff below.
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE); //need this also in case error message pops up
                if (task.isSuccessful()) { //if email and password matches FB, then successful task.
                    finish(); //stops the login activity so you don't go back to it when you're already logged in
                    progressBar.setVisibility(View.GONE); //make progress bar invisible
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class); //go to the dashboard
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //clears all the open activities
                    startActivity(intent); //starts the main activity
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
                } else { //print out actual exception
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() { //checks if user is logged in, then skip login screen
        super.onStart();
        if (mAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    //What the buttons will do
    @Override
    public void onClick(View v) {
        switch (v.getId()) { //to retrieve the ID to do the switch
            case R.id.login_button:
                userLogin();
                break;
            case R.id.goToSignUp_button:
                startActivity(new Intent(this, SignUpActivity.class));
                break;
            case R.id.forgotPasswordButton:
                startActivity(new Intent(this, ForgottenPasswordActivity.class));
                break;
        }
    }
}