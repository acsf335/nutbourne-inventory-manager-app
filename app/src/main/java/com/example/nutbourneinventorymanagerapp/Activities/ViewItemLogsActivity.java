package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemLog;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.ItemLogsListingAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

//This is the java class for viewing the list of all logs on an item, not the specific log. That is ViewLogActivity

public class ViewItemLogsActivity extends AppCompatActivity {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String LOG_ID = "com.example.nutbourneinventorymanagerapp.LOG_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    private String itemId;//the id of the item selected
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemLogsRef;
    private ItemLogsListingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item_logs);
        setTitle("Item logs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        itemId = intent.getStringExtra(ItemViewActivity.ITEM_ID);
        itemLogsRef = db.collection("Items").document(itemId).collection("Logs");
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        Query query = itemLogsRef.orderBy("timestamp", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<ItemLog> options = new FirestoreRecyclerOptions.Builder<ItemLog>()
                .setQuery(query, ItemLog.class).build();
        adapter = new ItemLogsListingAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewItemLogsListings);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ItemLogsListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openLog(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected item
    public void openLog(DocumentSnapshot documentSnapshot, int position) {
        String logId = documentSnapshot.getId(); //id of log selected
        //Start the activity and move the variables.
        Intent intent = new Intent(getApplicationContext(), ViewLogActivity.class);
        intent.putExtra(ITEM_ID, itemId);
        intent.putExtra(LOG_ID, logId);
        intent.putExtra(PAGE_CHECK, true);
        startActivity(intent);
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), ItemViewActivity.class);
                intent.putExtra(ITEM_ID, itemId);
                intent.putExtra(PAGE_CHECK, true);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}