package com.example.nutbourneinventorymanagerapp.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.StockRequests;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;

public class MakeStockRequestActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference stockReqColRef = db.collection("Stock Requests");
    private FirebaseUser currentUserInfo = FirebaseAuth.getInstance().getCurrentUser();
    //layout stuff
    private EditText editTextReason;
    private NumberPicker numberPickerQty;
    private TextView textViewItemName, textViewDate;
    private Button selectDateBtn, submitBtn;
    //fixed values to be given
    private String currentUserId, currentUserName;
    private String itemId, itemName;
    private String status, managerName, purchaserName, etaDate;
    private Timestamp timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_stock_request);

        //Changing the toolbar
        setTitle("Make a request");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getting item id and name from previous activity
        Intent intent = getIntent();
        itemId = intent.getStringExtra(ItemViewActivity.ITEM_ID);
        itemName = intent.getStringExtra(ItemViewActivity.ITEM_NAME);

        //user info
        currentUserId = currentUserInfo.getUid();
        currentUserName = currentUserInfo.getDisplayName();

        //layout stuff
        editTextReason = findViewById(R.id.mkReqReason);
        numberPickerQty = findViewById(R.id.mkReqQty);
        textViewItemName = findViewById(R.id.mkReqItemName);
        textViewDate = findViewById(R.id.mkReqDateSelected);
        selectDateBtn = findViewById(R.id.mkReqSelectDate);
        submitBtn = findViewById(R.id.mkReqSubmitBtn);

        numberPickerQty.setMinValue(1);
        numberPickerQty.setMaxValue(100);

        selectDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRequest();
            }
        });

        textViewItemName.setText(itemName);
    }

    //when pressing save
    private void submitRequest() {
        status = "Pending Approval";
        managerName = "Pending";
        purchaserName = "Pending";
        timestamp = Timestamp.now();
        etaDate = "Pending";

        String requiredDate = textViewDate.getText().toString();
        String reasonRequired = editTextReason.getText().toString();
        int qty = numberPickerQty.getValue();

        //Validation checks
        if (reasonRequired.trim().isEmpty()) {
            editTextReason.setError("Please give information about why you are making this request");
            editTextReason.requestFocus();
        }

        //Save to firebase
        stockReqColRef.add(new StockRequests(currentUserId, currentUserName, itemId, itemName, qty, requiredDate,
                timestamp, reasonRequired, status, managerName, purchaserName, etaDate));
        Toast.makeText(this, "Request submitted", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.make_stock_request_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.submit_request:
                submitRequest();
                return true;
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), ItemViewActivity.class);
                intent.putExtra(ITEM_ID, itemId);
                intent.putExtra(PAGE_CHECK, true);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Date stuff thanks to CodingWithMitch
    //method for starting the date picker
    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    //gives you the date selected in the dialog
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + (month + 1) + "/" + year;
        textViewDate.setText(date); //set the text to the date selected.
    }
}