package com.example.nutbourneinventorymanagerapp.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemLog;
import com.example.nutbourneinventorymanagerapp.Fragments.ItemFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddTakeOutItemLogActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    //for when you press the back toolbar button
    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    private String id; //Id of the document selected
    private String itemTitle;
    private String selectedLogType; //whether taken out or borrowed
    private Double originalQuantity; //the original quantity of the item selected
    //Layout items
    private TextView itemName;
    private NumberPicker itemQuantity;
    private Spinner logTypeSpinner;
    private Button selectedDateBtn;
    private TextView selectedDateText;
    private EditText itemTicket;
    private EditText itemDescription;
    private Button continueBtn;
    //Firebase stuff
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemsRef = db.collection("Items");
    private CollectionReference yourLogsRef = db.collection("Your Logs");
    private DocumentReference itemRef; //location of the item selected

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_take_out_item_log);

        //Changing toolbar
        setTitle("Taking out item");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        //Getting the values from the intent of ItemViewActivity
        Intent intent = getIntent();
        id = intent.getStringExtra(ItemFragment.ITEM_ID);

        itemName = findViewById(R.id.itemLogItemName);
        itemQuantity = findViewById(R.id.itemLogQuantity);
        logTypeSpinner = findViewById(R.id.itemLogType);
        selectedDateBtn = findViewById(R.id.itemLogBorrowedDateBtn);
        selectedDateText = findViewById(R.id.itemLogSelectedDate);
        itemTicket = findViewById(R.id.itemLogTicketNum);
        itemDescription = findViewById(R.id.itemLogDesc);
        continueBtn = findViewById(R.id.itemLogContinueBtn);
        //min and max values of the quantity selecter
        itemQuantity.setMinValue(1);
        itemQuantity.setMaxValue(100);

        //logTypeSpinner setup
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(getApplicationContext(), R.array.itemLogOptions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        logTypeSpinner.setAdapter(adapter); //set the adapter
        logTypeSpinner.setOnItemSelectedListener(this);

        //on click listener for the "select return date button"
        selectedDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        itemRef = itemsRef.document(id); //the document of this item selected

        //retrieve the data of the item (mainly its name) from the id given
        itemRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        itemTitle = document.getString("title"); //get the name of the item
                        originalQuantity = document.getDouble("quantity");
                        //put the data to the text views
                        itemName.setText(itemTitle);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    //Assigning the menu layout file to this activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_take_out_item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveItemLog:
                saveItemLog();
                return true;
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), ItemViewActivity.class);
                intent.putExtra(ITEM_ID, id);
                intent.putExtra(PAGE_CHECK, true);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //when pressing save
    private void saveItemLog() {
        //Get the data added
        String itemTitle = itemName.getText().toString();
        int quantity = itemQuantity.getValue();
        String logTypeSelected = selectedLogType;

        String returnDate = "";
        if (logTypeSelected.equals("Borrow")) {
            returnDate = selectedDateText.getText().toString();
        } else {
            if (logTypeSelected.equals("Taken Out")) {
                returnDate = "N/A";
            }
        }
        String logTicketNum = itemTicket.getText().toString();
        String logDescription = itemDescription.getText().toString();
        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String userName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        Timestamp timestamp = Timestamp.now();

        //Validation for saving
        if (logTypeSelected.equals("Borrow") && (returnDate.equals("Date"))) {
            Log.d("TAG", "Borrowed log type.");
            Toast.makeText(this, "Please select the return date before proceeding", Toast.LENGTH_SHORT).show();
            Log.d("TAG", "No return date selected!");
            return;
        }

        if (logDescription.trim().isEmpty()) {
            itemDescription.setError("Please give a detailed description of usage");
            itemDescription.requestFocus();
            return;
        }

        //Save to firebase in item subcollection.
        DocumentReference logRef = FirebaseFirestore.getInstance().collection("Items").document(id).collection("Logs").document();
        logRef.set(new ItemLog(itemTitle, quantity, logTypeSelected, returnDate, logTicketNum, logDescription,
                userID, userName, timestamp));
        String logDocId = logRef.getId(); //gets the id of this document being created
        Log.d("TAG", "ID of this new document is: " + logDocId);

        //Save another document with that doc info to the Your logs collection
        Map<String, Object> userLog = new HashMap<>();
        userLog.put("itemID", id);
        userLog.put("userID", userID);
        userLog.put("userName", userName);
        userLog.put("logID", logDocId);
        userLog.put("timestamp", timestamp);
        userLog.put("quantity", quantity);
        userLog.put("itemName", itemTitle);
        userLog.put("logType", logTypeSelected);
        yourLogsRef.add(userLog);

        //Change quantity (subtract from the amount taken here
        Double newQuantity = originalQuantity - quantity;
        itemRef.update("quantity", newQuantity);

        Toast.makeText(this, "Log has been added", Toast.LENGTH_SHORT).show();
        finish();
    }

    //Date stuff thanks to CodingWithMitch
    //method for starting the date picker
    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    //gives you the date selected in the dialog
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + (month + 1) + "/" + year;
        selectedDateText.setText(date); //set the text to the date selected.
    }

    //spinner stuff for itemLogType spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedLogType = parent.getItemAtPosition(position).toString();//set the selected item to a variable
        if (selectedLogType.equals("Taken Out")) {
            selectedDateBtn.setVisibility(View.GONE);
            selectedDateText.setVisibility(View.GONE);
        } else {
            if (selectedLogType.equals("Borrow")) {
                selectedDateBtn.setVisibility(View.VISIBLE);
                selectedDateText.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //LEAVE EMPTY
    }
}