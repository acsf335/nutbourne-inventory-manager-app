package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Dialogs.ReportDamagedItemDialog;
import com.example.nutbourneinventorymanagerapp.Dialogs.UpdateQuantityDialog;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.DamagedItem;
import com.example.nutbourneinventorymanagerapp.Fragments.ItemFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

//the back button on the toolbar is set in the Android Manifest file
//help on moving variables across from Coding in Flow

public class ItemViewActivity extends AppCompatActivity implements UpdateQuantityDialog.UpdateQuantityDialogListener, ReportDamagedItemDialog.ReportDamagedItemDialogListener {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String ITEM_NAME = "com.example.nutbourneinventorymanagerapp.ITEM_NAME";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    public String getIdFromMSRA, getIdFromItemFragment, getIdFromATOILA, getIdFromVILA, getIdFromIQLA;
    public Boolean getPageCheckFromMSRA, getPageCheckFromATOILA, getPageCheckFromVILA, getPageCheckFromIQLA;
    private String id;
    private String title;
    private String description;
    private Double quantity;
    private String price;
    private String imageUri;
    //page text views
    private TextView textViewTitle;
    private TextView textViewDescription;
    private TextView textViewQuantity;
    private TextView textViewPrice;
    private ImageView imageViewItem;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemsRef = db.collection("Items");
    private DocumentReference itemRef, damagedItemRef, damagedItemsFromHomeRef;
    private CollectionReference damagedItemsRef;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_view);

        Intent intent = getIntent();
        //Getting the values from the intent of ItemFragment
        getIdFromItemFragment = intent.getStringExtra(ItemFragment.ITEM_ID);
        id = intent.getStringExtra(ItemFragment.ITEM_ID);
        //Getting the values from the intent of MakeStockRequestActivity
        getIdFromMSRA = intent.getStringExtra(MakeStockRequestActivity.ITEM_ID);
        getPageCheckFromMSRA = intent.getBooleanExtra(MakeStockRequestActivity.PAGE_CHECK, false);
        //getting values from AddTakeOutItemLogActivity
        getIdFromATOILA = intent.getStringExtra(AddTakeOutItemLogActivity.ITEM_ID);
        getPageCheckFromATOILA = intent.getBooleanExtra(AddTakeOutItemLogActivity.PAGE_CHECK, false);
        //getting values from ViewItemLogsActivity
        getIdFromVILA = intent.getStringExtra(ViewItemLogsActivity.ITEM_ID);
        getPageCheckFromVILA = intent.getBooleanExtra(ViewItemLogsActivity.PAGE_CHECK, false);
        //getting values from ItemQuantityLogActivity
        getIdFromIQLA = intent.getStringExtra(ItemQuantityLogActivity.ITEM_ID);
        getPageCheckFromIQLA = intent.getBooleanExtra(ItemQuantityLogActivity.PAGE_CHECK, false);

        if (getPageCheckFromMSRA) {
            id = getIdFromMSRA;
        } else {
            if (getPageCheckFromATOILA) {
                id = getIdFromATOILA;
            } else {
                if (getPageCheckFromIQLA) {
                    id = getIdFromIQLA;
                } else {
                    if (getPageCheckFromVILA) {
                        id = getIdFromVILA;
                    } else {
                        id = getIdFromItemFragment;
                    }
                }
            }
        }

        //page text views
        textViewTitle = findViewById(R.id.itemName);
        textViewDescription = findViewById(R.id.description);
        textViewQuantity = findViewById(R.id.quantity);
        textViewPrice = findViewById(R.id.price);
        imageViewItem = findViewById(R.id.itemImageDisplay);

        itemRef = itemsRef.document(id);
        damagedItemsRef = itemRef.collection("Damaged Items");

        itemRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        title = document.getString("title");
                        description = document.getString("description");
                        quantity = document.getDouble("quantity");
                        price = document.getString("price");
                        imageUri = document.getString("imageData");
                        //put the data to the text views
                        textViewQuantity.setText(quantity + ""); //because it is an double
                        textViewTitle.setText(title);
                        textViewDescription.setText(description);
                        textViewPrice.setText(price);
                        textViewPrice.setText("£" + price);
                        if (price.isEmpty()) {
                            textViewPrice.setText("£0");
                        }
                        Picasso.get().load(imageUri).into(imageViewItem); //display the image
                        Log.d("tag", "Quantity obtained is: " + quantity);

                        setTitle(title); //set the toolbar heading to the name of the item
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    //Toolbar and the buttons stuff:-----------------------------------------------------------------
    //Linking the buttons in item_view_menu.xml

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_view_menu, menu);
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editItem:
                openItemEditor();
                return true;
            case R.id.updateQuantityPopupBtn:
                openDialog();
                return true;
            case R.id.viewQuantityLogs:
                openQuantityLogs();
                return true;
            case R.id.takeOutLog:
                openAddLogs();
                return true;
            case R.id.viewTakeOutLogs:
                openUsageLogs();
                return true;
            case R.id.makeStockRequest:
                openMakeStockRequest();
                return true;
            case R.id.reportDamaged:
                openReportDamaged();
                return true;
            case R.id.viewDamaged:
                openViewDamaged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //report damaged item dialog stuff
    public void openReportDamaged() {
        ReportDamagedItemDialog reportDamagedItemDialog = new ReportDamagedItemDialog();
        reportDamagedItemDialog.show(getSupportFragmentManager(), "Report a damaged item");
    }

    @Override
    public void createDamagedItem(String damagedDescription, int quantity) {
        String currentUserId = currentUser.getUid();
        String currentUserName = currentUser.getDisplayName();
        String status = "Damaged";
        String dateReported = Timestamp.now().toDate().toString();
        String dateRepaired = "Pending";
        String repairInfo = "Pending";

        //save in firebase item subcollection
        damagedItemRef = damagedItemsRef.document();
        damagedItemRef.set(new DamagedItem(id, title, status, damagedDescription, dateReported,
                dateRepaired, quantity, currentUserId, currentUserName, repairInfo));
        String damagedItemId = damagedItemRef.getId(); //get id of this newly created document

        //Save another document for Damaged Items Fragment
        damagedItemsFromHomeRef = db.collection("Damaged Items").document(damagedItemId);//setting id of the document to match id of the above one.

        Map<String, Object> damagedItemDoc = new HashMap<>();
        damagedItemDoc.put("itemID", id);
        damagedItemDoc.put("damagedItemID", damagedItemId);
        damagedItemDoc.put("itemName", title);
        damagedItemDoc.put("status", status);
        damagedItemDoc.put("dateReported", dateReported);
        damagedItemDoc.put("userName", currentUserName);
        damagedItemsFromHomeRef.set(damagedItemDoc);

        Toast.makeText(this, "Item reported as damaged.", Toast.LENGTH_SHORT).show();
    }

    public void openViewDamaged() {
        Intent intent = new Intent(getApplicationContext(), DamagedItemListings.class);
        intent.putExtra(ITEM_ID, id); //pass the id of the item selected
        startActivity(intent);
    }

    //dialog for updating the quantity
    public void openDialog() {
        UpdateQuantityDialog updateQuantityDialog = new UpdateQuantityDialog();
        updateQuantityDialog.show(getSupportFragmentManager(), "Update Quantity Dialog");
    }

    //does the updating when you click confirm
    @Override
    public void updateQuantity(int updatedQuantityValue, String updatedQuantityInfo) {
        CollectionReference updateQuantityOfItemRef = itemRef.collection("Quantity Updates");
        //adding the new quantity to the original for the new total
        Double newQuantity = quantity + updatedQuantityValue;
        itemRef.update("quantity", newQuantity); //update the quantity field of the item;
        textViewQuantity.setText(newQuantity + "");

        //create document in subcollection for history purposes
        Map<String, Object> addedQuantityLog = new HashMap<>();
        addedQuantityLog.put("itemId", id);
        addedQuantityLog.put("original_quantity", quantity);
        addedQuantityLog.put("added_quantity", updatedQuantityValue);
        addedQuantityLog.put("information", updatedQuantityInfo);
        addedQuantityLog.put("timestamp", FieldValue.serverTimestamp());
        addedQuantityLog.put("userStamp", FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        addedQuantityLog.put("userId", FirebaseAuth.getInstance().getCurrentUser().getUid());

        updateQuantityOfItemRef.add(addedQuantityLog);
        Toast.makeText(this, "Quantity added & updated", Toast.LENGTH_LONG).show();
    }

    //open the view quantity logs page
    public void openQuantityLogs() {
        Intent intent = new Intent(getApplicationContext(), ItemQuantityLogActivity.class);
        intent.putExtra(ITEM_ID, id); //give it the id of the document/item currently being viewed
        Log.d("log56", "This is the ID to be sent to ItemQuantityLogActivity" + id);
        startActivity(intent); //start the ItemQuantityLog activity
    }

    //open the taking out activity
    public void openAddLogs() {
        Intent intent = new Intent(getApplicationContext(), AddTakeOutItemLogActivity.class);
        intent.putExtra(ITEM_ID, id);
        startActivity(intent);
    }

    //opening the page to view all logs for the item
    public void openUsageLogs() {
        Intent intent = new Intent(getApplicationContext(), ViewItemLogsActivity.class);
        intent.putExtra(ITEM_ID, id);
        startActivity(intent);
    }

    public void openMakeStockRequest() {
        Intent intent = new Intent(getApplicationContext(), MakeStockRequestActivity.class);
        intent.putExtra(ITEM_ID, id);
        intent.putExtra(ITEM_NAME, title);
        startActivity(intent);
    }

    public void openItemEditor() {
        Intent intent = new Intent(getApplicationContext(), EditItemActivity.class);
        intent.putExtra(ITEM_ID, id);
        startActivity(intent);
    }
}