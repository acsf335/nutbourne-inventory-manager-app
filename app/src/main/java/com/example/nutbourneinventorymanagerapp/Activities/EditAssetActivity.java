package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class EditAssetActivity extends AppCompatActivity {

    private EditText editTextName, editTextDesc, editTextPrice, editTextUser, editTextAssetTag;
    private String assetCategoryId, assetId;
    private String originalName, originalDesc, originalPrice, originalUser, originalAssetTag;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference assetCatRef;
    private DocumentReference assetRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_asset);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("Edit Asset");

        editTextName = findViewById(R.id.editAssetName);
        editTextDesc = findViewById(R.id.editDescription);
        editTextPrice = findViewById(R.id.editPrice);
        editTextAssetTag = findViewById(R.id.editAssetTag);
        editTextUser = findViewById(R.id.editUser);

        Intent intent = getIntent();
        assetCategoryId = intent.getStringExtra(AssetViewActivity.ASSET_CATEGORY_NAME);
        assetId = intent.getStringExtra(AssetViewActivity.ASSET_ID);

        assetCatRef = db.collection("Assets").document(assetCategoryId).collection(assetCategoryId);
        assetRef = assetCatRef.document(assetId);

        assetRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        originalName = document.getString("title");
                        originalDesc = document.getString("description");
                        originalPrice = document.getString("price");
                        originalAssetTag = document.getString("assetTag");
                        originalUser = document.getString("assignedUser");
                        //put the data to the text views
                        editTextName.setText(originalName);
                        editTextDesc.setText(originalDesc);
                        editTextPrice.setText(originalPrice);
                        editTextAssetTag.setText(originalAssetTag);
                        editTextUser.setText(originalUser);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    public void saveChanges() {
        String updatedTitle = editTextName.getText().toString();
        String updatedDesc = editTextDesc.getText().toString();
        String updatedPrice = editTextPrice.getText().toString();
        String updatedAssetTag = editTextAssetTag.getText().toString();
        String updatedUser = editTextUser.getText().toString();

        //Validation for non-null values
        if (updatedTitle.trim().isEmpty() || updatedDesc.trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
            editTextName.setError("Please give the title");
            Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
            return;
        }

        assetRef.update("title", updatedTitle);
        assetRef.update("description", updatedDesc);
        assetRef.update("price", updatedPrice);
        assetRef.update("assetTag", updatedAssetTag);
        assetRef.update("assignedUser", updatedUser);

        Toast.makeText(this, "Changes saved", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_asset_menu, menu);
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_changes:
                saveChanges();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}