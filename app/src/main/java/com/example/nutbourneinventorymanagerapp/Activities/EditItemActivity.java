package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class EditItemActivity extends AppCompatActivity {

    private EditText editTextItemName, editTextDesc, editTextPrice;
    private NumberPicker editQuantity;

    private String itemId; //id of the item selected
    private String originalItemName, originalDescription, originalPrice;
    private Double originalQuantity;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemsRef = db.collection("Items");
    private DocumentReference itemRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        //Changing the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close); //setting the cross icon as the back button
        setTitle("Edit Item"); //changing name of activity which also changes toolbar name.

        Intent intent = getIntent();
        itemId = intent.getStringExtra(ItemViewActivity.ITEM_ID);

        editTextItemName = findViewById(R.id.editItemName);
        editTextDesc = findViewById(R.id.editItemDescription);
        editQuantity = findViewById(R.id.editQty);
        editTextPrice = findViewById(R.id.editItemPrice);

        itemRef = itemsRef.document(itemId);
        itemRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        originalQuantity = document.getDouble("quantity");
                        originalItemName = document.getString("title");
                        originalDescription = document.getString("description");
                        originalPrice = document.getString("price");
                        //put the data to the text views
                        editTextItemName.setText(originalItemName);
                        editTextDesc.setText(originalDescription);
                        editTextPrice.setText(originalPrice);
                        editQuantity.setMinValue(1);
                        editQuantity.setMaxValue(100);
                        String strOriginalQuantity = originalQuantity.toString();
                        String strOriginalQty = strOriginalQuantity.replace(".", "");
                        editQuantity.setValue(Integer.valueOf(strOriginalQty));
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    public void saveChanges() {
        //get the text from the edit texts and stuff
        String updatedItemName = editTextItemName.getText().toString();
        String updatedDesc = editTextDesc.getText().toString();
        String updatedPrice = editTextPrice.getText().toString();
        int updatedQty = editQuantity.getValue();

        //Validation for non-null values
        if (updatedItemName.trim().isEmpty() || updatedDesc.trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
            editTextItemName.setError("Please give the title");
            Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
            return;
        }
        //Set price to 0 if nothing is entered.
        if (updatedPrice.length() == 0) {
            editTextPrice.setText("0");
        }

        //Update the document
        itemRef.update("title", updatedItemName);
        itemRef.update("description", updatedDesc);
        itemRef.update("quantity", updatedQty);
        itemRef.update("price", updatedPrice);

        Toast.makeText(this, "Changes have been saved.", Toast.LENGTH_SHORT).show();
        finish(); //close and go back
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_item_menu, menu);
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_changes:
                saveChanges();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}