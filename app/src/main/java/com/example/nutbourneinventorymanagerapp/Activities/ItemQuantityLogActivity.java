package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemQuantityLog;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.ItemQuantityLogAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ItemQuantityLogActivity extends AppCompatActivity {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    private String id;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemQuantityRef;
    private ItemQuantityLogAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_quantity_log);
        setTitle("Quantity changes log");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id = intent.getStringExtra(ItemViewActivity.ITEM_ID);
        Log.d("Log1", "Here is the ID of document received: " + id);
        itemQuantityRef = db.collection("Items").document(id).collection("Quantity Updates");
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        Query query = itemQuantityRef.orderBy("timestamp", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<ItemQuantityLog> options = new FirestoreRecyclerOptions.Builder<ItemQuantityLog>()
                .setQuery(query, ItemQuantityLog.class).build();
        adapter = new ItemQuantityLogAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewItemQuantityLog);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //passing back the item id to prevent null document path crashing when pressing the back toolbar button
                Intent intent = new Intent(getApplicationContext(), ItemViewActivity.class);
                intent.putExtra(ITEM_ID, id);
                intent.putExtra(PAGE_CHECK, true);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}