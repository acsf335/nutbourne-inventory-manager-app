package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Dialogs.StockResponseNegativeDialog;
import com.example.nutbourneinventorymanagerapp.Dialogs.StockResponsePositiveDialog;
import com.example.nutbourneinventorymanagerapp.Fragments.RequestedItemsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.StockRequestsFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

//Java class for viewing an individual stock request by the purchaser/manager

public class ViewStockRequestActivity extends AppCompatActivity implements StockResponsePositiveDialog.StockResponsePositiveDialogListener, StockResponseNegativeDialog.StockResponseNegativeDialogListener {

    public static final String STOCKREQ_ID = "com.example.nutbourneinventorymanagerapp.STOCKREQ_ID";
    private String itemName, reqUserName, reqReason, status, managerName, managerInfo, purchaserName, purchaserInfo,
            requiredDate, timestamp, etaDate;
    private String itemId, userId, getStockRequestId, getRequestedItemId;
    private Double quantity;
    private Boolean getRequestedItemPC;
    private TextView textViewItemName, textViewQty, textViewUser, textViewRequiredDate, textViewDateMade,
            textViewReason, textViewStatus, textViewManager, textViewManagerInfo, textViewPurchaser, textViewPurchaserInfo,
            textViewEtaDate, textViewManagerHeader, textViewManagerInfoHeader, textViewPurchaserHeader, textViewPurchaserInfoHeader,
            textViewEtaDateHeader;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private DocumentReference stockReqRef;
    private FirebaseUser currentUserInfo = FirebaseAuth.getInstance().getCurrentUser(); //getting current user details
    private String currentUserId, currentUserName, currentUserRole;
    private String stockId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stock_request);

        //Getting values from StockRequestFragment
        Intent intent = getIntent();
        getStockRequestId = intent.getStringExtra(StockRequestsFragment.STOCKREQ_ID);
        getRequestedItemId = intent.getStringExtra(RequestedItemsFragment.STOCKREQ_ID);
        getRequestedItemPC = intent.getBooleanExtra(RequestedItemsFragment.PAGE_CHECK, false);

        if (getRequestedItemPC) { //if you came from the requested items page
            stockId = getRequestedItemId;
            Log.d("Tag", "I came from the requested items page");
        } else { //otherwise it is from stock requests page
            stockId = getStockRequestId;
            Log.d("Tag", "I came from the stock request page");
        }

        //getting current user info
        currentUserId = currentUserInfo.getUid();
        currentUserName = currentUserInfo.getDisplayName();

        //page text views assignments
        textViewItemName = findViewById(R.id.viewReqItemName);
        textViewQty = findViewById(R.id.viewReqQty);
        textViewUser = findViewById(R.id.viewReqUser);
        textViewRequiredDate = findViewById(R.id.viewReqDate);
        textViewDateMade = findViewById(R.id.viewReqDateReq);
        textViewReason = findViewById(R.id.viewReqReason);
        textViewStatus = findViewById(R.id.viewReqStatus);
        textViewManager = findViewById(R.id.viewReqManager);
        textViewManagerInfo = findViewById(R.id.viewReqManagerInfo);
        textViewPurchaser = findViewById(R.id.viewReqPurchaser);
        textViewPurchaserInfo = findViewById(R.id.viewReqPurchaserInfo);
        textViewEtaDate = findViewById(R.id.viewReqEta);

        textViewManagerHeader = findViewById(R.id.viewReqManagerHeader);
        textViewManagerInfoHeader = findViewById(R.id.viewReqManagerInfoHeader);
        textViewPurchaserHeader = findViewById(R.id.viewReqPurchaserHeader);
        textViewPurchaserInfoHeader = findViewById(R.id.viewReqPurchaserInfoHeader);
        textViewEtaDateHeader = findViewById(R.id.viewReqEtaDateHeader);

        stockReqRef = db.collection("Stock Requests").document(stockId);
        stockReqRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        itemId = document.getString("itemId");
                        itemName = document.getString("itemName");
                        quantity = document.getDouble("quantityReq");
                        reqUserName = document.getString("userName");
                        userId = document.getString("userId");
                        reqReason = document.getString("reasonReq");
                        status = document.getString("status");
                        managerName = document.getString("managerName");
                        purchaserName = document.getString("purchaserName");
                        requiredDate = document.getString("dateRequired");
                        timestamp = document.getTimestamp("timestamp").toDate().toString();
                        etaDate = document.getString("etaDate");
                        managerInfo = document.getString("managerAddInfo");
                        purchaserInfo = document.getString("purchaserAddInfo");

                        //put the data to the text views
                        textViewItemName.setText(itemName);
                        textViewQty.setText(String.valueOf(quantity));
                        textViewUser.setText(reqUserName);
                        textViewRequiredDate.setText(requiredDate);
                        textViewDateMade.setText(timestamp);
                        textViewReason.setText(reqReason);
                        textViewStatus.setText(status);
                        textViewManager.setText(managerName);
                        textViewPurchaser.setText(purchaserName);
                        textViewEtaDate.setText(etaDate);
                        textViewManagerInfo.setText(managerInfo);
                        textViewPurchaserInfo.setText(purchaserInfo);

                        //showing or hiding certain fields depending on the status
                        if (status.equals("Approved") || status.equals("Denied")) { //if status is from manager, then show manager stuff
                            textViewManagerHeader.setVisibility(View.VISIBLE);
                            textViewManager.setVisibility(View.VISIBLE);
                            textViewManagerInfoHeader.setVisibility(View.VISIBLE);
                            textViewManagerInfo.setVisibility(View.VISIBLE);
                        } else {
                            if (status.equals("Accepted") || status.equals("Rejected")) { //otherwise show purchase stuff
                                textViewManagerHeader.setVisibility(View.VISIBLE);
                                textViewManager.setVisibility(View.VISIBLE);
                                textViewManagerInfoHeader.setVisibility(View.VISIBLE);
                                textViewManagerInfo.setVisibility(View.VISIBLE);
                                textViewPurchaserHeader.setVisibility(View.VISIBLE);
                                textViewPurchaser.setVisibility(View.VISIBLE);
                                textViewPurchaserInfoHeader.setVisibility(View.VISIBLE);
                                textViewPurchaserInfo.setVisibility(View.VISIBLE);
                                textViewEtaDateHeader.setVisibility(View.VISIBLE);
                                textViewEtaDate.setVisibility(View.VISIBLE);
                            }
                        }
                        setTitle(itemName + " Request"); //set the toolbar heading to the name of the item
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });

        DocumentReference userRef = db.collection("Users").document(currentUserId);
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        currentUserRole = document.getString("role");
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    //Toolbar and the buttons stuff:-----------------------------------------------------------------
    //Linking the buttons in view_stock_request_menu.xml
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (!getRequestedItemPC) {
            inflater.inflate(R.menu.view_stock_request_menu, menu);
            Log.d("TAG!", "I came from the stock request page");
        } else {
            inflater.inflate(R.menu.view_stock_request_menu_two, menu);
            Log.d("TAG!", "I came from the requested items page");
        }
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.positiveResponse:
                openPosRespDialog();
                return true;
            case R.id.negativeResponse:
                openNegRespDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Method for opening the thumbs up dialog
    public void openPosRespDialog() {
        StockResponsePositiveDialog stockResponsePositiveDialog = new StockResponsePositiveDialog();
        stockResponsePositiveDialog.show(getSupportFragmentManager(), "Positive request Response");
    }

    //Method for opening the thumbs down dialog
    public void openNegRespDialog() {
        StockResponseNegativeDialog stockResponseNegativeDialog = new StockResponseNegativeDialog();
        stockResponseNegativeDialog.show(getSupportFragmentManager(), "Positive request Response");
    }

    //What to do when pressing confirm for positive Dialog box
    @Override
    public void applyRequestUpdate(String updatedStatus, String etaDate, String additionalInfo) {
        //update the values in the stock request document
        if (currentUserRole.equals("Purchaser") && !status.equals("Approved")) {
            Toast.makeText(this, "Requires Managers approval.", Toast.LENGTH_SHORT).show();
        } else {
            stockReqRef.update("status", updatedStatus);
            stockReqRef.update("etaDate", etaDate);
            if (updatedStatus.equals("Approved")) { //only manager or admin can get this status
                stockReqRef.update("managerAddInfo", additionalInfo); //.update adds the field even if it doesn't exist
                stockReqRef.update("managerName", currentUserName);
            } else {
                if (updatedStatus.equals("Accepted")) { //only purchaser can get this status
                    if (etaDate.equals("Date")) {
                        Toast.makeText(this, "Please provide an estimated date of fulfilment", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        stockReqRef.update("purchaserAddInfo", additionalInfo);
                        stockReqRef.update("purchaserName", currentUserName);
                    }
                }
            }
            Toast.makeText(this, "Status Updated", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void applyRequestNegativeUpdate(String updatedStatus, String reason) {
        //update the values in the stock request document
        stockReqRef.update("status", updatedStatus);
        if (status == "Denied") { //only manager or admin can get this status
            stockReqRef.update("managerAddInfo", reason);
            stockReqRef.update("managerName", currentUserName);
        } else {
            if (status == "Rejected") { //only purchaser can give this status
                stockReqRef.update("purchaserAddInfo", reason);
                stockReqRef.update("purchaserName", currentUserName);
                status = updatedStatus;
            }
        }
        Toast.makeText(this, "Status Updated", Toast.LENGTH_SHORT).show();
    }
}