package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Dialogs.ChangeUserRoleDialog;
import com.example.nutbourneinventorymanagerapp.Fragments.ManageUsersFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserViewActivity extends AppCompatActivity implements ChangeUserRoleDialog.ChangeUserRoleDialogListener {

    private String id, role;
    private Intent intent;
    private TextView userName, userEmailAddr, userRoleName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);

        //Getting the values from the intent of ManageUsersFragment
        intent = getIntent();
        id = intent.getStringExtra(ManageUsersFragment.USER_ID);
        String username = intent.getStringExtra(ManageUsersFragment.USER_NAME);
        String email = intent.getStringExtra(ManageUsersFragment.USER_EMAIL);
        role = intent.getStringExtra(ManageUsersFragment.USER_ROLE);

        setTitle(username); //setting header

        //layout variables
        userName = findViewById(R.id.userName);
        userEmailAddr = findViewById(R.id.userEmailAddr);
        userRoleName = findViewById(R.id.userRoleName);
        //setting the text views
        userName.setText(username);
        userEmailAddr.setText(email);
        userRoleName.setText(role);
    }

    //Option menu -------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_view_menu, menu);
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeUserRoleMenu:
                if (id.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    Toast.makeText(this, "Can't risk changing your own role.", Toast.LENGTH_SHORT).show();
                } else {
                    openDialog();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openDialog() {
        ChangeUserRoleDialog changeUserRoleDialog = new ChangeUserRoleDialog();
        changeUserRoleDialog.show(getSupportFragmentManager(), "Change Role Dialog");
    }

    //What we do when we click confirm in the dialog
    @Override
    public void applyRoleChange(String updateUserRole) {
        final DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(id);
        userRef.update("role", updateUserRole).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //change the role displayed - Doesn't work automatically
                role = intent.getStringExtra(ManageUsersFragment.USER_ROLE);
                userRoleName.setText(role);
                Toast.makeText(UserViewActivity.this, "Role updated successfully!", Toast.LENGTH_SHORT).show();
                Toast.makeText(UserViewActivity.this, "Go back.", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UserViewActivity.this, "Error updating user role", Toast.LENGTH_SHORT).show();
            }
        });
    }
}