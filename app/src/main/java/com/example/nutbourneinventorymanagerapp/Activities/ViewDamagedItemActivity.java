package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Dialogs.RepairDamagedItemDialog;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

//class to see more details about a damaged item once selected.
public class ViewDamagedItemActivity extends AppCompatActivity implements RepairDamagedItemDialog.RepairDamagedItemDialogListener {

    private TextView textViewName, textViewDesc, textViewQty, textViewStatus, textViewDateReported,
            textViewDateRepaired, textViewDateRepairedHeader, textViewUser, textViewRepairInfo, textViewRepairInfoHeader;
    private String itemName, description, status, dateReported, dateRepaired, user, repairInfo;
    private Double quantity;
    private String itemId, damagedItemId;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private DocumentReference damagedItemRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_damaged_item);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        itemId = intent.getStringExtra(DamagedItemListings.ITEM_ID);
        damagedItemId = intent.getStringExtra(DamagedItemListings.DAMAGEDITEM_ID);

        textViewName = findViewById(R.id.damagedItemDBName);
        textViewStatus = findViewById(R.id.damagedItemDBStatus);
        textViewQty = findViewById(R.id.damagedItemDBQty);
        textViewDesc = findViewById(R.id.damagedItemDesc);
        textViewDateReported = findViewById(R.id.damagedItemReportDate);
        textViewDateRepaired = findViewById(R.id.damagedItemRepairDate);
        textViewUser = findViewById(R.id.damagedItemUser);
        textViewRepairInfo = findViewById(R.id.damagedItemRepairInfo);
        textViewDateRepairedHeader = findViewById(R.id.damagedItemRepairDateHeader);
        textViewRepairInfoHeader = findViewById(R.id.damagedItemRepairInfoHeader);

        damagedItemRef = db.collection("Items").document(itemId).collection("Damaged Items")
                .document(damagedItemId);

        damagedItemRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        itemName = document.getString("itemName");
                        description = document.getString("description");
                        status = document.getString("status");
                        user = document.getString("userNameReported");
                        dateReported = document.getString("dateReported");
                        dateRepaired = document.getString("dateRepaired");
                        quantity = document.getDouble("quantity");
                        repairInfo = document.getString("repairInfo");
                        //set the fields to the text views
                        textViewName.setText(itemName);
                        textViewStatus.setText(status);
                        textViewQty.setText(String.valueOf(quantity));
                        textViewDesc.setText(description);
                        textViewDateReported.setText(dateReported);
                        textViewDateRepaired.setText(dateRepaired);
                        textViewUser.setText(user);
                        textViewRepairInfo.setText(repairInfo);

                        setTitle(itemName);
                        if (status.equals("Repaired")) {
                            textViewRepairInfo.setVisibility(View.VISIBLE);
                            textViewRepairInfoHeader.setVisibility(View.VISIBLE);
                            textViewDateRepaired.setVisibility(View.VISIBLE);
                            textViewDateRepairedHeader.setVisibility(View.VISIBLE);
                            textViewStatus.setTextColor(Color.GREEN);
                        } else {
                            textViewStatus.setTextColor(Color.RED);
                        }

                    } else {
                        Log.d("tag", "No such document muppet!");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    public void openRepairUpdate() {
        RepairDamagedItemDialog repairDamagedItemDialog = new RepairDamagedItemDialog();
        repairDamagedItemDialog.show(getSupportFragmentManager(), "Damaged item is repaired.");
    }

    @Override //when clicking confirm
    public void repairDamagedItem(String description, String date) {
        DocumentReference damagedItemFromHomeRef = db.collection("Damaged Items").document(damagedItemId);
        if (description.isEmpty()) {
            Toast.makeText(this, "Please give information about the repair", Toast.LENGTH_LONG).show();
            return;
        } else {
            if (date.equals("Date")) {
                Toast.makeText(this, "Please give the date the repair was completed", Toast.LENGTH_SHORT).show();
                return;
            } else {
                damagedItemRef.update("status", "Repaired");
                damagedItemRef.update("repairInfo", description);
                damagedItemRef.update("dateRepaired", date);
                //also update it from the damaged items collection
                damagedItemFromHomeRef.update("status", "Repaired");
                Toast.makeText(this, "Item is now repaired!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Assigning the menu layout file to this activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.damaged_item_view_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.updateToRepaired:
                openRepairUpdate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}