package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

//Got help to make this from Simplified Coding

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextEmail, editTextPassword, editTextDisplayName;
    ProgressBar progressBar;
    private FirebaseAuth mAuth; //declare instance

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Initialise
        mAuth = FirebaseAuth.getInstance();
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextDisplayName = findViewById(R.id.editTextDisplayName);
        progressBar = findViewById(R.id.progressBar);

        //attach listener to the buttons
        findViewById(R.id.signUp_button).setOnClickListener(this);
        findViewById(R.id.backToLogin_button).setOnClickListener(this);

        setTitle("Sign Up"); //change title of activity which changes name in toolbar
    }

    private void registerUser() {
        final String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        final String displayName = editTextDisplayName.getText().toString();
        //Validate the text entries
        if (email.isEmpty()) {
            editTextEmail.setError("Email is required!");
            editTextEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) { //if format is not email
            editTextEmail.setError("Please enter a valid email");
            editTextEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Password is required!");
            editTextPassword.requestFocus();
            return;
        }
        if (password.length() < 6) { //FB min pwd length is 6
            editTextPassword.setError("Minimum Password Length is 6.");
            editTextPassword.requestFocus();
            return;
        }
        if (displayName.isEmpty()) {
            editTextDisplayName.setError("Please enter your name.");
            editTextDisplayName.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //Toast.makeText(SignUpActivity.this, "Does this work?!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

                if (task.isSuccessful()) { //sign up successful
                    Toast.makeText(SignUpActivity.this, "Sign up success!", Toast.LENGTH_SHORT).show();
                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    UserProfileChangeRequest profileNameUpdates =
                            new UserProfileChangeRequest.Builder().setDisplayName(displayName).build();

                    user.updateProfile(profileNameUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //Michel helped me fix the signup issues
                                Toast.makeText(getApplicationContext(), "User name updated", Toast.LENGTH_SHORT).show();

                                //Getting this new user data and storing it in FireStore user collection
                                Map<String, Object> userData = new HashMap<>();
                                userData.put("displayName", displayName);
                                userData.put("emailAddress", email);
                                userData.put("role", "Employee"); //set all users to Employee role by default, which can be changed by Admin or through FB console

                                CollectionReference userRef = FirebaseFirestore.getInstance().collection("Users");
                                userRef.document(user.getUid()) //set the document id to be the user id and put the map into it
                                        .set(userData)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(SignUpActivity.this, "User added to system", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(SignUpActivity.this, "User not added to system", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                finish(); //stops the signup activity so you don't go back to it when you're already logged in
                                progressBar.setVisibility(View.GONE); //make progress bar invisible
                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class); //go to the dashboard
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //clears all the open activities
                                startActivity(intent); //starts the main activity
                                Toast.makeText(getApplicationContext(), "Account registration successful!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignUpActivity.this, "FAILURE!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) { //if email is already used
                        Toast.makeText(getApplicationContext(), "This email is already registered.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp_button:
                registerUser();
                break;
            case R.id.backToLogin_button:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }
}