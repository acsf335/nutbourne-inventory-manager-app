package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.DamagedItem;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.DamagedItemsAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

//The listings of damaged items viewed from within the item and not the damaged item fragment.
public class DamagedItemListings extends AppCompatActivity {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String DAMAGEDITEM_ID = "com.example.nutbourneinventorymanagerapp.DAMAGEDITEM_ID";
    private String itemId; //id of the item selected
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemRef = db.collection("Items");
    private CollectionReference damagedItemsRef;
    private DamagedItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_damaged_item_listings);
        setTitle("Damaged Item Logs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        itemId = intent.getStringExtra(ItemViewActivity.ITEM_ID); //the id of the item selected.
        damagedItemsRef = itemRef.document(itemId).collection("Damaged Items");

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        Query query = damagedItemsRef.orderBy("dateReported", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<DamagedItem> options = new FirestoreRecyclerOptions.Builder<DamagedItem>()
                .setQuery(query, DamagedItem.class).build();

        adapter = new DamagedItemsAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.recyclerDamagedItemListings);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new DamagedItemsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openDamagedItem(documentSnapshot, position);
            }
        });
    }

    public void openDamagedItem(DocumentSnapshot documentSnapshot, int position) {
        String damagedItemId = documentSnapshot.getId();

        Intent intent = new Intent(getApplicationContext(), ViewDamagedItemActivity.class);
        intent.putExtra(ITEM_ID, itemId);
        intent.putExtra(DAMAGEDITEM_ID, damagedItemId);
        startActivity(intent);
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }
}