package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.AssetListing;
import com.example.nutbourneinventorymanagerapp.Fragments.AssetsFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

//Class for adding a new asset.

public class NewAssetActivity extends AppCompatActivity {
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private EditText editTextAssetName;
    private EditText editTextDescription;
    private EditText editTextPrice;
    private EditText editTextAssetTag;
    private EditText editTextUser;
    private Button mButtonChooseImage;
    private Button mButtonTakePicture;
    private ImageView mItemImageView;
    private ProgressBar mProgressBar;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference assetRef;
    private StorageReference storageRef;
    private Uri mImageUri;
    private String uploadedImageUri;
    private StorageTask mUploadTask;
    private Bitmap imageBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_asset);

        //Get collection location from AssetFragment
        Intent intent = getIntent();
        String categoryLocation = intent.getStringExtra(AssetsFragment.CATEGORY_LOCATION);
        Log.d("log6", "This is the category location in NewAssetActivity: " + categoryLocation);
        assetRef = db.collection("Assets").document(categoryLocation).collection(categoryLocation);

        //Changing the toolbar
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close); //setting the cross icon as the back button
        setTitle("Add Asset"); //changing name of activity which also changes toolbar name.

        editTextAssetName = findViewById(R.id.editTextAssetName);
        editTextDescription = findViewById(R.id.editTextDescription);
        editTextPrice = findViewById(R.id.editTextPrice);
        editTextAssetTag = findViewById(R.id.editTextAssetTag);
        editTextUser = findViewById(R.id.editTextUser);


        //image variables
        mButtonChooseImage = findViewById(R.id.chooseImageBtn);
        mButtonTakePicture = findViewById(R.id.takePictureBtn);
        mItemImageView = findViewById(R.id.assetImageView);
        mProgressBar = findViewById(R.id.asset_progress_bar);

        storageRef = FirebaseStorage.getInstance().getReference("asset_uploads");

        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        mButtonTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });
    }

    //Assigning the menu xml to be the menu for this activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_asset_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //handle on click events for the toolbar buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_changes:
                if (mUploadTask != null && mUploadTask.isInProgress()) {
                    Toast.makeText(this, "Upload in Progress", Toast.LENGTH_SHORT).show();
                } else {
                    uploadImage();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //action when pressing save
    private void saveNewAsset() {
        //Get the data added
        String itemName = editTextAssetName.getText().toString();
        String description = editTextDescription.getText().toString();
        String price = editTextPrice.getText().toString();
        String assetTag = editTextAssetTag.getText().toString();
        String assignedUser = editTextUser.getText().toString();
        String imageData = uploadedImageUri;

        //Validation for non-null values
        if (itemName.trim().isEmpty() || description.trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
            Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (assignedUser.trim().isEmpty() || assetTag.trim().isEmpty()) {
                Toast.makeText(this, "Please enter the asset tag number & assigned user, or enter 'None'",
                        Toast.LENGTH_LONG).show();
                return;
            }
        }

        //Save to Firebase. '.add' auto generates the id of the document
        assetRef.add(new AssetListing(itemName, description, price, assetTag, assignedUser, imageData)); //same as what we passed to the recycler adapter
        Toast.makeText(this, "Asset saved", Toast.LENGTH_SHORT).show();
        finish();
    }

    //Methods for selecting or taking an image and then uploading them
    //Methods for choosing an image from device ------------------
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    //Methods for taking a picture
    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK //if user selected an image
                && data != null && data.getData() != null) {
            mImageUri = data.getData(); //uri of the image we picked to be uploaded & displayed
            Picasso.get().load(mImageUri).into(mItemImageView); //display selected image in the ImageView
        } else {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) { //for using camera
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                mItemImageView.setImageBitmap(imageBitmap);
            }
        }
    }

    //Methods for uploading the image file -------------
    private String getFileExtension(Uri uri) { //returns the extension of the file (.jpg or .png etc..)
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadImage() {
        if (mImageUri != null) { //if user picked a file
            final String fileLocation = System.currentTimeMillis() + "." + getFileExtension(mImageUri);
            final StorageReference fileReference = storageRef.child(fileLocation); //creates a random name for the file based on the time and extension

            if (editTextAssetName.getText().toString().trim().isEmpty() ||
                    editTextDescription.getText().toString().trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
                Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
                return;
            }

            mUploadTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { //when upload was successful
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(0);
                                } //delays the progess bar reset to 0 so user sees it go to 100% when successful
                            }, 500);
                            Log.d("myImageLog", "Image Upload Successful");
                            //for getting the URI of the image you just uploaded so it gets added to the URI field of the asset.
                            Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    return fileReference.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if (task.isSuccessful()) {
                                        uploadedImageUri = task.getResult().toString();
                                        saveNewAsset();
                                    } else {
                                        Toast.makeText(NewAssetActivity.this, "Error uploading", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() { //when upload failed
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(NewAssetActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() { //when upload is in progress
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount()); //gives us progress as a number
                            mProgressBar.setProgress((int) progress);
                        }
                    });

        } else {
            if (imageBitmap != null) {
                Toast.makeText(this, "Camera mode", Toast.LENGTH_SHORT).show();
            } else {
                //if user didn't take a pic or select one
                Toast.makeText(this, "Get a photo damnit!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}