package com.example.nutbourneinventorymanagerapp.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.nutbourneinventorymanagerapp.Dialogs.CreateCategoryDialog;
import com.example.nutbourneinventorymanagerapp.Dialogs.DialogChangeDisplayName;
import com.example.nutbourneinventorymanagerapp.Fragments.AssetFoldersFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.AssetsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.DamagedItemsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.DashboardFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.ItemFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.ManageAccountFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.ManageUsersFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.RequestedItemsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.SettingsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.StockRequestsFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.TakenOutFragment;
import com.example.nutbourneinventorymanagerapp.Fragments.YourLogsFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        DialogChangeDisplayName.DialogChangeDisplayNameListener, CreateCategoryDialog.CreateCategoryDialogListener, AssetFoldersFragment.AssetFoldersFragmentListener {

    private DrawerLayout drawer;
    private TextView loggedInUser; //This represents the user name shown in the nav header
    private TextView userRole; //This is the role shown on the nav drawer header

    private AssetsFragment assetsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setting the toolbar (nav) as the actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Dashboard");

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        assetsFragment = new AssetsFragment();

        //Creates the hamburger icon on top left to open the nav bar
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState(); //takes care of rotating the hamburger icon when you click

        //To show the user display name and role  on the header
        View headerView = navigationView.getHeaderView(0); //to link the header from navigation_header.xml
        loggedInUser = headerView.findViewById(R.id.logged_in_user);
        userRole = headerView.findViewById(R.id.user_role);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(user.getUid());
        if (user != null) {
            loggedInUser.setText(user.getDisplayName());
            //userRole.setText(userRef.get("role"));
            setUserRole(userRole); //to set the user role in the nav header
        } else {
            loggedInUser.setText("No user name yet!");
        }

        //Sets the dashboard as the first page which shows when you launch the app
        if (savedInstanceState == null) { //if statement prevents this page showing again if you just rotate the device resulting in destroying the process
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new DashboardFragment()).commit();
            navigationView.getMenu().getItem(0).setChecked(true);
        }

        hideItem(navigationView);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) { //handles the clicks on the navigation drawer
        switch (item.getItemId()) { //have a case for each button on nav drawer
            case R.id.dashboard:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DashboardFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.items:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ItemFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.assets:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AssetFoldersFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.damaged_items:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DamagedItemsFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.requested_items:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RequestedItemsFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.taken_out:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TakenOutFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.your_logs:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new YourLogsFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.stock_requests:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new StockRequestsFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.manage_users:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ManageUsersFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
            case R.id.settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SettingsFragment()).addToBackStack(null).commit(); //when you press the button, it will open the page
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true; //true means an item is selected after clicking it
    }

    @Override
    public void onBackPressed() { //for pressing back button to just close the nav drawer instead of exiting.
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START); //so if the drawer is open when you press back, it closes the nav drawer
        } else {
            super.onBackPressed(); //if not open, then just close the activity as usual
        }
    }

    //DialogChangeDisplayName - What happens when you click confirm
    @Override
    public void applyChanges(final String displayName) { //the actual action from pressing confirm
        //To update the display name when you press confirm.

        final ManageAccountFragment maf = new ManageAccountFragment();

        final TextView userDisplayName;
        //userDisplayName = findViewById(R.id.displayNameView);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(displayName).build();
        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Display name updated.", Toast.LENGTH_SHORT).show();
                    maf.setValue(user.getDisplayName()); //update the display name text view on ManageAccount screen (doesn't work)
                    loggedInUser.setText(user.getDisplayName()); //update name in the nav drawer header
                    //update the displayName in the DB
                    CollectionReference userRef = FirebaseFirestore.getInstance().collection("Users");
                    userRef.document(user.getUid()).update("displayName", displayName);
                }
            }
        });
    }

    //CreateCategoryDialog - What happens when you click confirm
    @Override
    public void createCategory(String categoryName) {
        CollectionReference assetCatRef = FirebaseFirestore.getInstance().collection("Assets");
        CollectionReference categoryRef = assetCatRef.document(categoryName).collection(categoryName);

        Map<String, Object> categoryData = new HashMap<>();
        categoryData.put("category", categoryName);

        assetCatRef.document(categoryName).set(categoryData);
    }

    //interfaces for sending category data between asset folder fragment and assets fragment
    @Override
    public void onCategorySent(CharSequence category) {
        Log.d("log4", "Running onCategorySent in MainActivity.Java");
        assetsFragment.updateSelectedCategory(category);
        Log.d("log1", "This is what is to be passed as the category: " + category);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                assetsFragment, "AssetsFragment").addToBackStack(null).commit();
    }

    //to set the user role text on the header
    public void setUserRole(final TextView userRole) {
        //final TextView role = userRole;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(user.getUid());
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        //role.setText(document.getString("role"));
                        userRole.setText(document.getString("role"));

                    }
                }
            }
        });
    }

    //Hide Nav drawer menu items based on role
    private void hideItem(NavigationView navigationView) {
        final MenuItem ManageUsersMenu = navigationView.getMenu().findItem(R.id.manage_users);
        final MenuItem StockRequestsMenu = navigationView.getMenu().findItem(R.id.stock_requests);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(user.getUid());
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        String roleUser = document.getString("role");
                        if (roleUser.equals("Employee")) {
                            ManageUsersMenu.setVisible(false);
                            StockRequestsMenu.setVisible(false);
                        } else {
                            if (roleUser.equals("Purchaser") || roleUser.equals("Manager")) {
                                ManageUsersMenu.setVisible(false);
                            }
                        }
                    }
                }
            }
        });
    }
}