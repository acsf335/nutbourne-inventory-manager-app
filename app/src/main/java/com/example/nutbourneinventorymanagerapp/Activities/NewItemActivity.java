package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemListing;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class NewItemActivity extends AppCompatActivity {
    private static final int PICK_IMAGE_REQUEST = 1; //stores data on the image we selected so we can keep it
    private static final int REQUEST_IMAGE_CAPTURE = 1; //stores data on photo we take
    private EditText editTextItemName;
    private EditText editTextDescription;
    private NumberPicker numberPickerQuantity;
    private EditText editTextPrice;
    private Button mButtonChooseImage;
    private Button mButtonTakePicture;
    private TextView mTextViewShowUploads;
    private ImageView mItemImageView;
    private ProgressBar mProgressBar;
    private Uri mImageUri; //points to the image
    private Bitmap imageBitmap; //for taking a photo
    private String uploadedImageUri;

    //Firebase Storage stuff
    private StorageReference storageRef;
    private CollectionReference collectionRef;

    private StorageTask mUploadTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);

        //Changing the toolbar
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close); //setting the cross icon as the back button
        setTitle("Add Item"); //changing name of activity which also changes toolbar name.

        editTextItemName = findViewById(R.id.editTextItemName);
        editTextDescription = findViewById(R.id.editTextDescription);
        numberPickerQuantity = findViewById(R.id.numberPickerQuantity);
        editTextPrice = findViewById(R.id.editTextPrice);

        //image variables
        mButtonChooseImage = findViewById(R.id.btnChooseImage);
        mButtonTakePicture = findViewById(R.id.btnTakePicture);
        mTextViewShowUploads = findViewById(R.id.txtShowUploads);
        mItemImageView = findViewById(R.id.itemImageView);
        mProgressBar = findViewById(R.id.progress_bar);

        storageRef = FirebaseStorage.getInstance().getReference("item_uploads"); //save in folder called item_uploads
        collectionRef = FirebaseFirestore.getInstance().collection("Items"); //the collection

        //onClick listeners:
        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser(); //method for opening file chooser
            }
        });

        mButtonTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        mTextViewShowUploads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Probably can remove
            }
        });

        //set min and max value of quantity
        numberPickerQuantity.setMinValue(1);
        numberPickerQuantity.setMaxValue(1000);
    }

    //Assigning the menu xml to be the menu for this activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //handle on click events for the toolbar buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_changes:
                if (mUploadTask != null && mUploadTask.isInProgress()) { //prevents spamming and duplicate image uploads
                    Toast.makeText(this, "Upload in Progress", Toast.LENGTH_SHORT).show();
                } else {
                    uploadFile();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //action when pressing save
    private void saveNewItem() {
        //Get the data added
        String itemName = editTextItemName.getText().toString();
        String description = editTextDescription.getText().toString();
        int quantity = numberPickerQuantity.getValue();
        String price = editTextPrice.getText().toString();
        //UploadImage imageData = upload;
        String imageData = uploadedImageUri;

        //Validation for non-null values
        if (itemName.trim().isEmpty() || description.trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
            editTextItemName.setError("Please give the title");
            Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
            return;
        }
        //Set price to 0 if nothing is entered.
        if (price.length() == 0) {
            editTextPrice.setText("0");
        }

        //Save to Firebase. '.add' auto generates the id of the document
        CollectionReference itemRef = FirebaseFirestore.getInstance().collection("Items"); //the collection
        itemRef.add(new ItemListing(itemName, description, quantity, price, imageData)); //same as what we passed to the recycler adapter
        Toast.makeText(this, "Item saved", Toast.LENGTH_SHORT).show();
        finish();
    }

    //Methods for selecting or taking an image and then uploading them
    //Methods for choosing an image from device ------------------
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    //Methods for taking a picture
    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    //For selecting and displaying the selected image once we choose it in the gallery or capture it
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) { //called when we pick the file
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK //if user selected an image
                && data != null && data.getData() != null) {
            mImageUri = data.getData(); //uri of the image we picked to be uploaded & displayed
            Picasso.get().load(mImageUri).into(mItemImageView); //display selected image in the ImageView
        } else {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) { //for using camera
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                mItemImageView.setImageBitmap(imageBitmap);
            }
        }
    }

    //Methods for uploading the image file -------------
    private String getFileExtension(Uri uri) { //returns the extension of the file (.jpg or .png etc..)
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        if (mImageUri != null) { //if user picked a file
            final String fileLocation = System.currentTimeMillis() + "." + getFileExtension(mImageUri);
            final StorageReference fileReference = storageRef.child(fileLocation); //creates a random name for the file based on the time and extension

            if (editTextItemName.getText().toString().trim().isEmpty() ||
                    editTextDescription.getText().toString().trim().isEmpty()) { //use of .trim to also see an empty space as an empty string preventing single space as populated
                Toast.makeText(this, "Please insert the name and description", Toast.LENGTH_SHORT).show();
                return;
            }

            mUploadTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { //when upload was successful
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(0);
                                } //delays the progess bar reset to 0 so user sees it go to 100% when successful
                            }, 500);
                            Log.d("myImageLog", "Image Upload Successful");
                            //for getting the URI of the image you just uploaded so it gets added to the URI field of the item
                            Task<Uri> urlTask = mUploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    return fileReference.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if (task.isSuccessful()) {
                                        uploadedImageUri = task.getResult().toString();
                                        saveNewItem();
                                    } else {
                                        Toast.makeText(NewItemActivity.this, "Error uploading", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() { //when upload failed
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(NewItemActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() { //when upload is in progress
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount()); //gives us progress as a number
                            mProgressBar.setProgress((int) progress);
                        }
                    });

        } else {
            if (imageBitmap != null) {
                Toast.makeText(this, "Camera mode", Toast.LENGTH_SHORT).show();
            } else {
                //if user didn't take a pic or select one
                Toast.makeText(this, "Get a photo damnit!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}