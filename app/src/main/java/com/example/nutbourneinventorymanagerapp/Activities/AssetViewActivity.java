package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Fragments.AssetsFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.squareup.picasso.Picasso;

//the back button on the toolbar is set in the Android Manifest file
//help on moving variables across from Coding in Flow

public class AssetViewActivity extends AppCompatActivity {

    public static final String ASSET_CATEGORY_NAME = "com.example.nutbourneinventorymanagerapp.ASSET_CATEGORY_NAME";
    public static final String ASSET_ID = "com.example.nutbourneinventorymanagerapp.ASSET_ID";
    private String assetId, categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_view);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        //Getting the values from the intent of ItemFragment
        Intent intent = getIntent();
        assetId = intent.getStringExtra(AssetsFragment.ASSET_ID);
        String title = intent.getStringExtra(AssetsFragment.ASSET_TITLE);
        String description = intent.getStringExtra(AssetsFragment.ASSET_DESCRIPTION);
        String price = intent.getStringExtra(AssetsFragment.ASSET_PRICE);
        String tag = intent.getStringExtra(AssetsFragment.ASSET_TAG);
        String assignedUser = intent.getStringExtra(AssetsFragment.ASSET_USER);
        String imageUri = intent.getStringExtra(AssetsFragment.ASSET_IMAGEURI);
        categoryName = intent.getStringExtra(AssetsFragment.CATEGORY_LOCATION);

        setTitle(title); //set the toolbar heading to the name of the item

        //page text views
        TextView textViewTitle = findViewById(R.id.assetName);
        TextView textViewDescription = findViewById(R.id.assetDescription);
        TextView textViewPrice = findViewById(R.id.assetPrice);
        TextView textViewTag = findViewById(R.id.assetTagNumber);
        TextView textViewUser = findViewById(R.id.assetAssignedUser);
        ImageView imageViewAsset = findViewById(R.id.assetImageDisplay);

        //assign the values to the text views
        textViewTitle.setText(title);
        textViewDescription.setText(description);
        textViewPrice.setText("£" + price);
        if (price.isEmpty()) {
            textViewPrice.setText("£0");
        }
        textViewTag.setText(tag);
        textViewUser.setText(assignedUser);
        Picasso.get().load(imageUri).into(imageViewAsset);
    }


    public void openEditAsset() {
        Intent intent = new Intent(getApplicationContext(), EditAssetActivity.class);
        intent.putExtra(ASSET_CATEGORY_NAME, categoryName);
        intent.putExtra(ASSET_ID, assetId);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.asset_view_menu, menu);
        return true;
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editAsset:
                openEditAsset();
            default:
                return true;
        }
    }
}