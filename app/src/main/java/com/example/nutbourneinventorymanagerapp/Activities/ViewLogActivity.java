package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Fragments.YourLogsFragment;
import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

//This is the java class for viewing the the specific log you click on in ViewItemLogsActivity

public class ViewLogActivity extends AppCompatActivity {

    //fields
    String itemName, description, logType, ticketNum, userName, timestamp, returnDate;
    Double quantity;
    //for getting from ViewItemLogsActivity
    private String getItemLogItemId;
    private String getItemLogLogId;
    private Boolean isItemLogPage;
    //for getting from YourLogsFragment
    private String getYourLogsItemId;
    private String getYourLogsLogId;
    private Boolean isYourLogPage;
    private String itemId;
    private String logId;
    //Layout textviews
    private TextView textViewItemName, textViewDesc, textViewLogType, textViewTicketNum, textViewUserName, textViewTimestamp,
            textViewReturnDate, textViewQuantity, textViewReturnDateHeader;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemsRef = db.collection("Items");
    private DocumentReference logRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_log);

        //get the id variables from previous activities depending on which you came from
        Intent intent = getIntent();
        getItemLogItemId = intent.getStringExtra(ViewItemLogsActivity.ITEM_ID);
        getItemLogLogId = intent.getStringExtra(ViewItemLogsActivity.LOG_ID);
        isItemLogPage = intent.getBooleanExtra(ViewItemLogsActivity.PAGE_CHECK, false);

        getYourLogsItemId = intent.getStringExtra(YourLogsFragment.ITEM_ID);
        getYourLogsLogId = intent.getStringExtra(YourLogsFragment.LOG_ID);
        isYourLogPage = intent.getBooleanExtra(YourLogsFragment.PAGE_CHECK, false);

        if (isYourLogPage) { //if the user came from YourLogActivity
            itemId = getYourLogsItemId;
            logId = getYourLogsLogId;
            Log.d("TAG", "Came from Your logs");
            Log.d("TAG", "itemID here is: " + itemId);
            Log.d("TAG", "logID here is: " + logId);
        } else { //else they came from the itemLogsActivity
            itemId = getItemLogItemId;
            logId = getItemLogLogId;
            Log.d("TAG", "Came from ViewItemLogsActivity");
            Log.d("TAG", "itemID here is: " + itemId);
            Log.d("TAG", "logID here is: " + logId);
        }

        //layout text views assignments
        textViewItemName = findViewById(R.id.logViewItemName);
        textViewDesc = findViewById(R.id.logViewDescription);
        textViewLogType = findViewById(R.id.logViewLogType);
        textViewTicketNum = findViewById(R.id.logViewTicket);
        textViewUserName = findViewById(R.id.logViewUser);
        textViewTimestamp = findViewById(R.id.logViewDate);
        textViewReturnDate = findViewById(R.id.logViewReturnDate);
        textViewQuantity = findViewById(R.id.logViewQuantity);
        textViewReturnDateHeader = findViewById(R.id.logViewReturnDateHeader);

        logRef = itemsRef.document(itemId).collection("Logs").document(logId);
        logRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        //retrieve the fields data
                        itemName = document.getString("itemName");
                        description = document.getString("description");
                        logType = document.getString("logType");
                        ticketNum = document.getString("ticketNumber");
                        returnDate = document.getString("returnDate");
                        userName = document.getString("userName");
                        timestamp = document.getTimestamp("timestamp").toDate().toString();
                        quantity = document.getDouble("quantity");
                        //set the fields to the textviews
                        textViewItemName.setText(itemName);
                        textViewDesc.setText(description);
                        textViewLogType.setText(logType);
                        if (ticketNum.isEmpty()) {
                            textViewTicketNum.setText("N/A");
                        } else {
                            textViewTicketNum.setText(ticketNum);
                        }

                        textViewReturnDate.setText(returnDate);
                        textViewTimestamp.setText(timestamp);
                        textViewUserName.setText(userName);
                        textViewQuantity.setText(quantity + "");

                        if (logType.equals("Taken Out")) {
                            textViewReturnDate.setVisibility(View.GONE);
                            textViewReturnDateHeader.setVisibility(View.GONE);
                        }

                        setTitle(itemName + " Log"); //set toolbar title
                    } else {
                        Log.d("tag", "No such document muppet!");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }
}