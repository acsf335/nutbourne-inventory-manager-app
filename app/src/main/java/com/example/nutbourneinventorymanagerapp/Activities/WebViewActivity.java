package com.example.nutbourneinventorymanagerapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.example.nutbourneinventorymanagerapp.Fragments.ManageUsersFragment;
import com.example.nutbourneinventorymanagerapp.R;

public class WebViewActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        setTitle("Firebase Console");
        //getting the URL from whichever page we came from
        Intent intent = getIntent();
        String url = intent.getStringExtra(ManageUsersFragment.URL);

        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    //Let the user press back button to go back in the website
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) { //checks if we can go back in the web
            webView.goBack(); //then go back to the last page
        } else {
            super.onBackPressed(); //close app
        }
    }
}