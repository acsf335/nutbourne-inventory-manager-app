package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.StockRequests;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class StockRequestsAdapter extends FirestoreRecyclerAdapter<StockRequests, StockRequestsAdapter.StockRequestsHolder> {
    private OnItemClickListener listener;

    public StockRequestsAdapter(@NonNull FirestoreRecyclerOptions<StockRequests> options) {
        super(options);
    }

    //what data goes in each card
    @Override
    protected void onBindViewHolder(@NonNull StockRequestsHolder holder, int position, @NonNull StockRequests model) {
        holder.textViewItemName.setText(model.getItemName());
        holder.textViewUser.setText(model.getUserName());
        holder.textViewStatus.setText(model.getStatus());
        holder.textViewDate.setText(String.valueOf(model.getDateRequired()));
        if (model.getStatus().equals("Accepted")) {
            holder.textViewStatus.setTextColor(Color.GREEN);
        } else {
            if (model.getStatus().equals("Approved")) {
                holder.textViewStatus.setTextColor(Color.BLUE);
            } else {
                if (model.getStatus().equals("Rejected") || model.getStatus().equals("Denied")) {
                    holder.textViewStatus.setTextColor(Color.RED);
                }
            }
        }
    }

    @NonNull
    @Override
    public StockRequestsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_stock_requests_recycler,
                viewGroup, false);
        return new StockRequestsHolder(view);
    }

    public void setOnItemClickListener(StockRequestsAdapter.OnItemClickListener listener) {//used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class StockRequestsHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewUser;
        TextView textViewStatus;
        TextView textViewDate;

        public StockRequestsHolder(@NonNull View itemView) {
            super(itemView);
            //textviews in the cards
            textViewItemName = itemView.findViewById(R.id.listReqItemName);
            textViewUser = itemView.findViewById(R.id.listReqUsername);
            textViewStatus = itemView.findViewById(R.id.listReqStatus);
            textViewDate = itemView.findViewById(R.id.listReqRequiredDate);

            //for clicking a card
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}