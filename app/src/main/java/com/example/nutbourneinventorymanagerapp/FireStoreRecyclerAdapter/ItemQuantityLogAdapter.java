package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemQuantityLog;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class ItemQuantityLogAdapter extends FirestoreRecyclerAdapter<ItemQuantityLog, ItemQuantityLogAdapter.ItemQuantityLogHolder> {

    public ItemQuantityLogAdapter(@NonNull FirestoreRecyclerOptions<ItemQuantityLog> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ItemQuantityLogHolder holder, int position, @NonNull ItemQuantityLog model) {
        holder.tvOriginalQuantity.setText(String.valueOf(model.getOriginalQuantity()));
        holder.tvAddedQuantity.setText(String.valueOf(model.getAddedQuantity()));
        holder.tvInformation.setText(model.getInformation());
        holder.tvTimestamp.setText(String.valueOf(model.getTimeStamp()));
        holder.tvUserStamp.setText(model.getUserStamp());
    }

    @NonNull
    @Override
    public ItemQuantityLogHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_quantity_log_recycler,
                viewGroup, false);
        return new ItemQuantityLogHolder(view);
    }

    class ItemQuantityLogHolder extends RecyclerView.ViewHolder {
        TextView tvOriginalQuantity;
        TextView tvAddedQuantity;
        TextView tvInformation;
        TextView tvTimestamp;
        TextView tvUserStamp;

        public ItemQuantityLogHolder(@NonNull View itemView) {
            super(itemView);
            tvOriginalQuantity = itemView.findViewById(R.id.originalQuantityValue);
            tvAddedQuantity = itemView.findViewById(R.id.addedQuantityValue);
            tvInformation = itemView.findViewById(R.id.quantityLogInfo);
            tvTimestamp = itemView.findViewById(R.id.quantityLogDate);
            tvUserStamp = itemView.findViewById(R.id.quantityLogUser);
        }
    }
}