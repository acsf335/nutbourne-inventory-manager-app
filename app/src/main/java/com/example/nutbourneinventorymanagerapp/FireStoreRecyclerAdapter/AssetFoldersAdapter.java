package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.AssetFolders;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class AssetFoldersAdapter extends FirestoreRecyclerAdapter<AssetFolders, AssetFoldersAdapter.AssetFoldersHolder> {
    private OnItemClickListener listener;

    public AssetFoldersAdapter(@NonNull FirestoreRecyclerOptions<AssetFolders> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull AssetFoldersHolder holder, int position, @NonNull AssetFolders model) {
        holder.textViewCategory.setText(model.getCategory());
    }

    @NonNull
    @Override
    public AssetFoldersHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asset_folders_recycler,
                viewGroup, false);
        return new AssetFoldersHolder(view);
    }

    public void setOnItemClickListener(AssetFoldersAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    class AssetFoldersHolder extends RecyclerView.ViewHolder {
        TextView textViewCategory;

        public AssetFoldersHolder(@NonNull View itemView) {
            super(itemView);
            textViewCategory = itemView.findViewById(R.id.assetCategoryName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}