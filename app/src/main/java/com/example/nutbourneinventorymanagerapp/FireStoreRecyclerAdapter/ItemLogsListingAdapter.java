package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemLog;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class ItemLogsListingAdapter extends FirestoreRecyclerAdapter<ItemLog, ItemLogsListingAdapter.ItemLogsListingHolder> {
    private OnItemClickListener listener;

    public ItemLogsListingAdapter(@NonNull FirestoreRecyclerOptions<ItemLog> options) {
        super(options);
    }

    //what data goes in each card
    @Override
    protected void onBindViewHolder(@NonNull ItemLogsListingHolder holder, int position, @NonNull ItemLog model) {
        holder.textViewLogDate.setText(String.valueOf(model.getTimestamp()));
        holder.textViewLogQty.setText(String.valueOf(model.getQuantity()));
        holder.textViewLogType.setText(model.getLogType());
        holder.textViewLogUser.setText(model.getUserName());
    }

    @NonNull
    @Override
    public ItemLogsListingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_logs_listing_recycler,
                viewGroup, false);
        return new ItemLogsListingHolder(view);
    }

    public void setOnItemClickListener(ItemLogsListingAdapter.OnItemClickListener listener) {//used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class ItemLogsListingHolder extends RecyclerView.ViewHolder {
        TextView textViewLogDate;
        TextView textViewLogQty;
        TextView textViewLogUser;
        TextView textViewLogType;

        public ItemLogsListingHolder(@NonNull View itemView) {
            super(itemView);
            //text views in the cards
            textViewLogDate = itemView.findViewById(R.id.logDate);
            textViewLogQty = itemView.findViewById(R.id.logQuantity);
            textViewLogUser = itemView.findViewById(R.id.logUser);
            textViewLogType = itemView.findViewById(R.id.logType);

            //for clicking a card
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}