package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.DamagedItem;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class DamagedItemsAdapter extends FirestoreRecyclerAdapter<DamagedItem, DamagedItemsAdapter.DamagedItemHolder> {
    private OnItemClickListener listener;

    public DamagedItemsAdapter(@NonNull FirestoreRecyclerOptions<DamagedItem> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull DamagedItemHolder holder, int position, @NonNull DamagedItem model) {
        holder.textViewStatus.setText(model.getStatus());
        holder.textViewQty.setText(String.valueOf(model.getQuantity()));
        holder.textViewDateReported.setText(model.getDateReported());
    }

    @NonNull
    @Override
    public DamagedItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.damaged_items_list_recycler,
                viewGroup, false);
        return new DamagedItemHolder(view);
    }

    public void setOnItemClickListener(DamagedItemsAdapter.OnItemClickListener listener) { //used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class DamagedItemHolder extends RecyclerView.ViewHolder {
        TextView textViewStatus, textViewQty, textViewDateReported;

        public DamagedItemHolder(@NonNull View itemView) {
            super(itemView);
            textViewStatus = itemView.findViewById(R.id.damagedItemStatus);
            textViewQty = itemView.findViewById(R.id.damagedItemQty);
            textViewDateReported = itemView.findViewById(R.id.damagedItemReported);

            //for clicking a card
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}