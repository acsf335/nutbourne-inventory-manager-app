package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.DamagedItemsDashboard;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

//this is for viewing from the dashboard the damaged items from the damaged items collection

public class DamagedItemsDashboardAdapter extends FirestoreRecyclerAdapter<DamagedItemsDashboard, DamagedItemsDashboardAdapter.DamagedItemsDashboardHolder> {
    private OnItemClickListener listener;

    public DamagedItemsDashboardAdapter(@NonNull FirestoreRecyclerOptions<DamagedItemsDashboard> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull DamagedItemsDashboardAdapter.DamagedItemsDashboardHolder holder, int position, @NonNull DamagedItemsDashboard model) {
        holder.textViewItemName.setText(model.getItemName());
        holder.textViewStatus.setText(model.getStatus());
        holder.textViewQty.setText(String.valueOf(model.getQuantity()));
        holder.textViewDateReported.setText(model.getDateReported());
        if ((model.getStatus()).equals("Repaired")) {
            holder.textViewStatus.setTextColor(Color.GREEN);
        } else {
            holder.textViewStatus.setTextColor(Color.RED);
        }
    }

    @NonNull
    @Override
    public DamagedItemsDashboardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.damaged_items_dashboard_list_recycler,
                viewGroup, false);
        return new DamagedItemsDashboardHolder(view);
    }

    public void setOnItemClickListener(DamagedItemsDashboardAdapter.OnItemClickListener listener) { //used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class DamagedItemsDashboardHolder extends RecyclerView.ViewHolder {
        TextView textViewStatus, textViewQty, textViewDateReported, textViewItemName;

        public DamagedItemsDashboardHolder(@NonNull View itemView) {
            super(itemView);
            textViewStatus = itemView.findViewById(R.id.damagedItemDBStatus);
            textViewQty = itemView.findViewById(R.id.damagedItemDBQty);
            textViewDateReported = itemView.findViewById(R.id.damagedItemDBReported);
            textViewItemName = itemView.findViewById(R.id.damagedItemDBName);

            //for clicking a card
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}