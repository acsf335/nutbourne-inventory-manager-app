package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemListing;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

public class ItemListingAdapter extends FirestoreRecyclerAdapter<ItemListing, ItemListingAdapter.ItemListingHolder> {
    private OnItemClickListener listener;

    //Constructor
    public ItemListingAdapter(@NonNull FirestoreRecyclerOptions<ItemListing> options) {
        super(options);
    }

    //What goes in each card
    @Override
    protected void onBindViewHolder(@NonNull ItemListingHolder holder, int position, @NonNull ItemListing model) {
        holder.textViewTitle.setText(model.getTitle());
        holder.textViewDescription.setText(model.getDescription());
        holder.textViewQuantity.setText(String.valueOf(model.getQuantity()));
        Picasso.get().load(model.getImageData()).fit().centerCrop().into(holder.imageViewIcon);
    }

    //Tell the adapter which layout to inflate
    @NonNull
    @Override
    public ItemListingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_listing_recycler,
                viewGroup, false);
        return new ItemListingHolder(view);
    }

    public void setOnItemClickListener(ItemListingAdapter.OnItemClickListener listener) {//used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    //The ViewHolder
    class ItemListingHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewDescription;
        TextView textViewQuantity;
        ImageView imageViewIcon;

        public ItemListingHolder(@NonNull View itemView) { //constructor for AssetListingHolder
            super(itemView); //itemView is the card we click itself
            textViewTitle = itemView.findViewById(R.id.assetName);
            textViewDescription = itemView.findViewById(R.id.briefDescription);
            textViewQuantity = itemView.findViewById(R.id.quantity);
            imageViewIcon = itemView.findViewById(R.id.itemIcon);

            //On click listeners so the cards are buttons
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}