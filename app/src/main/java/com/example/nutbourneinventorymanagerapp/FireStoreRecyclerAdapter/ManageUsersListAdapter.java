package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ManageUsersList;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class ManageUsersListAdapter extends FirestoreRecyclerAdapter<ManageUsersList, ManageUsersListAdapter.ManageUsersListHolder> {
    private OnItemClickListener listener;

    //constructor
    public ManageUsersListAdapter(@NonNull FirestoreRecyclerOptions<ManageUsersList> options) {
        super(options);
    }

    //What goes in each card
    @Override
    protected void onBindViewHolder(@NonNull ManageUsersListHolder holder, int position, @NonNull ManageUsersList model) {
        holder.textViewUsername.setText(model.getDisplayName());
        holder.textViewEmail.setText(model.getEmailAddress());
        holder.textViewRole.setText(model.getRole());
    }

    @NonNull
    @Override
    public ManageUsersListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_users_recycler,
                viewGroup, false);
        return new ManageUsersListHolder(view);
    }

    public void setOnItemClickListener(ManageUsersListAdapter.OnItemClickListener listener) {//used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    //The ViewHolder
    class ManageUsersListHolder extends RecyclerView.ViewHolder {
        TextView textViewUsername;
        TextView textViewEmail;
        TextView textViewRole;

        //constructor
        public ManageUsersListHolder(@NonNull View itemView) {
            super(itemView);
            textViewUsername = itemView.findViewById(R.id.userName);
            textViewEmail = itemView.findViewById(R.id.userEmail);
            textViewRole = itemView.findViewById(R.id.userRole);

            //Onclick listener for the cards
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}