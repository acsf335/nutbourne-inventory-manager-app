package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.YourLogs;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class YourLogsAdapter extends FirestoreRecyclerAdapter<YourLogs, YourLogsAdapter.YourLogsHolder> {
    private OnItemClickListener listener;

    public YourLogsAdapter(@NonNull FirestoreRecyclerOptions<YourLogs> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull YourLogsHolder holder, int position, @NonNull YourLogs model) {
        holder.textViewItemName.setText(model.getItemName());
        holder.textViewQuantity.setText(String.valueOf(model.getQuantity()));
        holder.textViewDate.setText(String.valueOf(model.getTimestamp()));
        holder.textViewType.setText(model.getLogType());
    }

    @NonNull
    @Override
    public YourLogsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.your_logs_list_recycler,
                viewGroup, false);
        return new YourLogsHolder(view);
    }

    public void setOnItemClickListener(YourLogsAdapter.OnItemClickListener listener) { //used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class YourLogsHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;
        TextView textViewQuantity;
        TextView textViewDate;
        TextView textViewType;

        public YourLogsHolder(@NonNull View itemView) {
            super(itemView);
            textViewItemName = itemView.findViewById(R.id.yourLogsItemName);
            textViewQuantity = itemView.findViewById(R.id.yourLogsQty);
            textViewDate = itemView.findViewById(R.id.yourLogsDate);
            textViewType = itemView.findViewById(R.id.yourLogsType);

            //for clicking a card
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}