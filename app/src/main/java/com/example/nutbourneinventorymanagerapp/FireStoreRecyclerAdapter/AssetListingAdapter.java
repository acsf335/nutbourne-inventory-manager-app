package com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.AssetListing;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

//Adapter is used to retrieve the data from the DB and present it in the App recycler view

public class AssetListingAdapter extends FirestoreRecyclerAdapter<AssetListing, AssetListingAdapter.AssetListingHolder> {
    private OnItemClickListener listener;

    public AssetListingAdapter(@NonNull FirestoreRecyclerOptions<AssetListing> options) { //constructor
        super(options);
    }

    //What we want to put in each view of our card layout. AKA what goes into the cards
    @Override
    protected void onBindViewHolder(@NonNull AssetListingHolder holder, int position, @NonNull AssetListing model) {
        holder.textViewTitle.setText(model.getTitle()); //get the title and put it in the textViewTitle variable
        holder.textViewDescription.setText(model.getDescription());
        holder.textViewAssetTag.setText(model.getAssetTag());
        Picasso.get().load(model.getImageData()).fit().centerCrop().into(holder.imageViewIcon);
    }

    //Tells the adapter which layout to inflate
    @NonNull
    @Override
    public AssetListingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asset_listing_recycler,
                viewGroup, false);
        return new AssetListingHolder(view);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {//used in the activity to set this as the onclicklistener for the adapter
        this.listener = listener;
    }

    //To send the click from this adapter to the activity we want to send the data to
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position); //what we are sending to the activity
    }

    class AssetListingHolder extends RecyclerView.ViewHolder { //ViewHolder class
        TextView textViewTitle;
        TextView textViewDescription;
        TextView textViewAssetTag;
        ImageView imageViewIcon;

        public AssetListingHolder(@NonNull View itemView) { //constructor for AssetListingHolder
            super(itemView); //itemView is the card we click itself
            textViewTitle = itemView.findViewById(R.id.assetName);
            textViewDescription = itemView.findViewById(R.id.briefDescription);
            textViewAssetTag = itemView.findViewById(R.id.assetTagNum);
            imageViewIcon = itemView.findViewById(R.id.assetIcon);

            //Setting the on click listener to the card itself so we know which we clicked
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) { //prevents crashes from position = null or -1
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }
}