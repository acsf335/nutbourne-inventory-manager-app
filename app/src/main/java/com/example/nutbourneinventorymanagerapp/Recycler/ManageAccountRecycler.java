package com.example.nutbourneinventorymanagerapp.Recycler;

public class ManageAccountRecycler {
    private String mHeading; //the heading of the info. e.g. email address:
    private String mText; //The info from the DB

    public ManageAccountRecycler(String heading, String text) {
        mHeading = heading;
        mText = text;
    }

    public String getText() {
        return mText;
    }

    public String getHeading() {
        return mHeading;
    }
}