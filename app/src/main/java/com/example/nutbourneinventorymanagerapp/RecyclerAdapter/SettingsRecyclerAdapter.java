package com.example.nutbourneinventorymanagerapp.RecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.R;
import com.example.nutbourneinventorymanagerapp.Recycler.SettingsRecycler;

import java.util.ArrayList;

public class SettingsRecyclerAdapter extends RecyclerView.Adapter<SettingsRecyclerAdapter.SettingsViewHolder> {
    private ArrayList<SettingsRecycler> mSettingsList;
    private OnItemClickListener mListener;

    public SettingsRecyclerAdapter(ArrayList<SettingsRecycler> settingsList) { //getting the data from the list
        mSettingsList = settingsList;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public SettingsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.settings_recycler, viewGroup, false);
        SettingsViewHolder svh = new SettingsViewHolder(view, mListener);
        return svh;
    }

    //get the text and images from the array list and show it
    @Override
    public void onBindViewHolder(@NonNull SettingsViewHolder holder, int position) {
        SettingsRecycler currentItem = mSettingsList.get(position);
        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextView1.setText(currentItem.getText1());
        holder.mTextView2.setText(currentItem.getText2());
    }

    @Override
    public int getItemCount() {
        return mSettingsList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class SettingsViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;

        public SettingsViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.iconView);
            mTextView1 = itemView.findViewById(R.id.headingLine);
            mTextView2 = itemView.findViewById(R.id.subheadingLine);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}