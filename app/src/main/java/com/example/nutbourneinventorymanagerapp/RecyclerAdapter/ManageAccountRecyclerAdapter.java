package com.example.nutbourneinventorymanagerapp.RecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.R;
import com.example.nutbourneinventorymanagerapp.Recycler.ManageAccountRecycler;

import java.util.ArrayList;

public class ManageAccountRecyclerAdapter extends RecyclerView.Adapter<ManageAccountRecyclerAdapter.ManageAccountViewHolder> {

    private ArrayList<ManageAccountRecycler> mManageList;
    private OnItemClickListener mListener;

    public ManageAccountRecyclerAdapter(ArrayList<ManageAccountRecycler> manageAccountList) {
        mManageList = manageAccountList;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ManageAccountViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_account_recycler, viewGroup,
                false);
        ManageAccountViewHolder mavh = new ManageAccountViewHolder(view, mListener);
        return mavh;
    }

    @Override
    public void onBindViewHolder(@NonNull ManageAccountViewHolder holder, int position) {
        ManageAccountRecycler currentItem = mManageList.get(position);
        holder.mHeading.setText(currentItem.getHeading());
        holder.mTextView.setText(currentItem.getText());
    }

    @Override
    public int getItemCount() {
        return mManageList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position); //for all items listed
    }

    public static class ManageAccountViewHolder extends RecyclerView.ViewHolder {

        public TextView mHeading; //heading of the data
        public TextView mTextView; //the user info

        public ManageAccountViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mHeading = itemView.findViewById(R.id.textViewHeading);
            mTextView = itemView.findViewById(R.id.textViewInfo);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}