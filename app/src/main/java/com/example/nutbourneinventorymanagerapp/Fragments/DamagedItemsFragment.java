package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.ViewDamagedItemActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.DamagedItemsDashboard;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.DamagedItemsDashboardAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class DamagedItemsFragment extends Fragment {
    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String DAMAGEDITEM_ID = "com.example.nutbourneinventorymanagerapp.DAMAGEDITEM_ID";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference damagedItemsRef = db.collection("Damaged Items");
    private DamagedItemsDashboardAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.damaged_items_fragment, container, false);
        getActivity().setTitle("Damaged Items"); //change title of activity which changes name in toolbar

        setUpRecyclerView(view);

        return view;
    }

    private void setUpRecyclerView(View view) {
        Query query = damagedItemsRef.orderBy("dateReported", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<DamagedItemsDashboard> options = new FirestoreRecyclerOptions.Builder<DamagedItemsDashboard>()
                .setQuery(query, DamagedItemsDashboard.class).build();

        adapter = new DamagedItemsDashboardAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewDamagedItems);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new DamagedItemsDashboardAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openDamagedItem(documentSnapshot, position);
            }
        });
    }

    private void openDamagedItem(DocumentSnapshot documentSnapshot, int position) {
        final String damagedItemId = documentSnapshot.getId();

        DocumentReference damagedItemListingRef = damagedItemsRef.document(damagedItemId);
        damagedItemListingRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        String itemId = document.getString("itemID");
                        //Start the activity and move the variables.
                        Intent intent = new Intent(getActivity(), ViewDamagedItemActivity.class);
                        intent.putExtra(ITEM_ID, itemId);
                        intent.putExtra(DAMAGEDITEM_ID, damagedItemId);
                        startActivity(intent);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }
}