package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.LoginActivity;
import com.example.nutbourneinventorymanagerapp.Dialogs.DialogChangeDisplayName;
import com.example.nutbourneinventorymanagerapp.R;
import com.example.nutbourneinventorymanagerapp.Recycler.ManageAccountRecycler;
import com.example.nutbourneinventorymanagerapp.RecyclerAdapter.ManageAccountRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class ManageAccountFragment extends Fragment {

    public String displayName, displayEmail, userRole;

    private ArrayList<ManageAccountRecycler> mManageList;
    private RecyclerView mRecyclerView;
    private ManageAccountRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_account, container, false);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            displayName = (user.getDisplayName());
            displayEmail = (user.getEmail());
            getUserRole();
        }

        createManageList();
        buildRecyclerView(view);
        getActivity().setTitle("Manage Account"); //change title of activity which changes name in toolbar
        return view;
    }

    public void setValue(String newName) {
        displayName = newName;
    }

    public void openDialog() { //open dialog box to edit name
        DialogChangeDisplayName dialogChangeDisplayName = new DialogChangeDisplayName();
        dialogChangeDisplayName.show(getFragmentManager(), "Change name dialog");
    }

    private void createManageList() {
        getUserRole();
        mManageList = new ArrayList<>();
        mManageList.add(new ManageAccountRecycler("Display Name:", displayName));
        mManageList.add(new ManageAccountRecycler("Email:", displayEmail));
        mManageList.add(new ManageAccountRecycler("Role:", userRole));
        mManageList.add(new ManageAccountRecycler("Reset Password", "Click to reset your password"));
        mManageList.add(new ManageAccountRecycler("Log Out", "Log out of your account."));
    }

    private void buildRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerViewManageAccount);
        mRecyclerView.setHasFixedSize(true); //if recyclerview won't change in size regardless of items, then this increases performance.
        mLayoutManager = new LinearLayoutManager(getActivity()); //Coding in Flow said write "this" but this is a fragment!
        mAdapter = new ManageAccountRecyclerAdapter(mManageList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        getUserRole();

        mAdapter.setOnItemClickListener(new ManageAccountRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                switch (position) {
                    case 0:
                        openDialog();
                        break;
                    case 2:
                        getUserRole();
                        Toast.makeText(getActivity(), "Your role is: " + userRole, Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ResetPasswordFragment()).addToBackStack(null).commit();
                        break;
                    case 4:
                        FirebaseAuth.getInstance().signOut(); //sign out
                        startActivity(new Intent(getActivity(), LoginActivity.class)); //go back to login screen
                        //have to use getActivity() instead of 'this' as this is a fragment
                        Toast.makeText(getActivity(), "Signed out", Toast.LENGTH_SHORT).show();
                        getActivity().finish(); //finish this activity to prevent going back when logged out.
                        break;
                }
            }
        });
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        getUserRole();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);
        return;
    }

    //to get just the user role field
    public void getUserRole() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(user.getUid());
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        userRole = document.getString("role");
                    }
                }
            }
        });
    }
}