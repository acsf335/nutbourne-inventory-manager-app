package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Dialogs.CreateCategoryDialog;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.AssetFolders;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.AssetFoldersAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class AssetFoldersFragment extends Fragment {

    //variables for passing the data to other activity
    public static final String ASSET_CATEGORY_NAME = "com.example.nutbourneinventorymanagerapp.ASSET_CATEGORY_NAME";
    //Member variables to link the AssetListing recycler to the adapter
    private FirebaseFirestore db = FirebaseFirestore.getInstance(); //get connection to firestore
    private CollectionReference assetRef = db.collection("Assets"); //reference to the asset collection in firestore
    private AssetFoldersAdapter adapter;
    private AssetFoldersFragmentListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.asset_folders_fragment, container, false);

        setUpRecyclerView(view); //passing view in here so I can use findViewById in this method
        getActivity().setTitle("Assets"); //change title of activity which changes name in toolbar

        return view;
    }

    //to link the recycler view to the adapter
    private void setUpRecyclerView(View view) {
        Query query = assetRef.orderBy("category", Query.Direction.ASCENDING); //orders by title
        //Getting the query into the adapter
        FirestoreRecyclerOptions<AssetFolders> options = new FirestoreRecyclerOptions.Builder<AssetFolders>()
                .setQuery(query, AssetFolders.class).build();

        adapter = new AssetFoldersAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewAssetFolders);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        //implements the OnItemClickListener interface in the Adapter
        adapter.setOnItemClickListener(new AssetFoldersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                //What we want to do with the snapshot. I want to open new activity
                openCategory(documentSnapshot, position);
            }
        });
    }

    //method to move variables to AssetView activity and open it
    public void openCategory(DocumentSnapshot documentSnapshot, int position) {
        String category = documentSnapshot.getString("category");
        String categoryId = documentSnapshot.getId();
        //Start the fragment and move the variables

        listener.onCategorySent(category); //send the category to the main activity
        Log.d("log2", "This is category to be sent to the activity: " + category);
        Log.d("log5", "Running openCategory in AssetFoldersFragment.java");
        Log.d("log6", "listener.onCategorySent(category) in AssetFoldersFragment");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AssetFoldersFragmentListener) { //check if the main activity implements this interface
            listener = (AssetFoldersFragmentListener) context; //assign activity to listener
        } else {
            throw new RuntimeException(context.toString() + " must implement AssetFoldersFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null; //for when killing the fragment
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }

    //These 3 methods pertain to the toolbar and their buttons.
    //For changing the toolbar buttons
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    //Giving the options menu for the toolbar
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.asset_folder_menu, menu);
        return;
    }

    //on click listener for the options menu in toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //item is used to check what was clicked
        switch (item.getItemId()) {
            case R.id.add_category:
                openDialog(); //start the create category dialog
                return true;
            case R.id.option1:
                Toast.makeText(getActivity(), "Option 1 selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openDialog() { //open dialog box to edit name
        CreateCategoryDialog createCategoryDialog = new CreateCategoryDialog();
        createCategoryDialog.show(getFragmentManager(), "Change name dialog");
    }

    //interface for sending category to main activity
    public interface AssetFoldersFragmentListener {
        void onCategorySent(CharSequence category);
    }
}