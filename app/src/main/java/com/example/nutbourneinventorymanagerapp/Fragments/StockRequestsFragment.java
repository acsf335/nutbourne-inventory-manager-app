package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.ViewStockRequestActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.StockRequests;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.StockRequestsAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class StockRequestsFragment extends Fragment {

    public static final String STOCKREQ_ID = "com.example.nutbourneinventorymanagerapp.STOCKREQ_ID";
    //FireStore recycler stuff
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference stockRequestsRef = db.collection("Stock Requests"); //The link to the FSDB collection
    private StockRequestsAdapter adapter;
    private CollectionReference usersRef = db.collection("Users");
    private DocumentReference userRef;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    private String currentUserId = currentUser.getUid();
    private String currentUserRole;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stock_requests_fragment, container, false);
        getActivity().setTitle("Stock Requests"); //change title of activity which changes name in toolbar
        //get user role to show relevant stocks
        userRef = usersRef.document(currentUserId);
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("tag", "DocumentSnapshot data: " + document.getData());
                        currentUserRole = document.getString("role");
                    } else {
                        Log.d("tag", "No such document");
                        currentUserRole = document.getString("role");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
        setUpRecyclerView(view);
        return view;
    }

    //FireStore Recycler stuff
    //Link the Item Recyclerview to the adapter
    private void setUpRecyclerView(View view) {
        Query query = stockRequestsRef.orderBy("dateRequired", Query.Direction.ASCENDING);

        FirestoreRecyclerOptions<StockRequests> options = new FirestoreRecyclerOptions.Builder<StockRequests>()
                .setQuery(query, StockRequests.class).build();

        adapter = new StockRequestsAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewStockRequests);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        //implements the OnItemClickListener interface in the Adapter
        adapter.setOnItemClickListener(new StockRequestsAdapter.OnItemClickListener() {
            @Override //what to do with that card's data
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openRequest(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected request
    public void openRequest(DocumentSnapshot documentSnapshot, int position) {
        String id = documentSnapshot.getId(); //ID of the item selected
        //Move variables to next activity
        Intent intent = new Intent(getActivity(), ViewStockRequestActivity.class);
        intent.putExtra(STOCKREQ_ID, id);
        startActivity(intent);
    }

    //To tell the adapter to start and stop listening to database changes to avoid wasting resources when app is in background
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}