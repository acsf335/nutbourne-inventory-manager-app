package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.UserViewActivity;
import com.example.nutbourneinventorymanagerapp.Activities.WebViewActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ManageUsersList;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.ManageUsersListAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ManageUsersFragment extends Fragment {

    //Variables for moving to another activity --Help from Coding in flow
    public static final String USER_ID = "com.example.nutbourneinventorymanagerapp.USER_ID";
    public static final String USER_NAME = "com.example.nutbourneinventorymanagerapp.USER_NAME";
    public static final String USER_EMAIL = "com.example.nutbourneinventorymanagerapp.USER_EMAIL";
    public static final String USER_ROLE = "com.example.nutbourneinventorymanagerapp.USER_ROLE";
    public static final String URL = "com.example.nutbourneinventorymanagerapp.URL";
    //FireStore Recycler stuff
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference usersRef = db.collection("Users");
    private ManageUsersListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.manage_users_fragment, container, false);

        setUpRecyclerView(view);
        getActivity().setTitle("Manage Users"); //change title of activity which changes name in toolbar
        return view;
    }

    //FireStore Recycler stuff
    //Link the Item Recyclerview to the adapter
    private void setUpRecyclerView(View view) {
        Query query = usersRef.orderBy("displayName", Query.Direction.ASCENDING);
        //Getting the query into the adapter
        FirestoreRecyclerOptions<ManageUsersList> options = new FirestoreRecyclerOptions.Builder<ManageUsersList>()
                .setQuery(query, ManageUsersList.class).build();

        adapter = new ManageUsersListAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewManageUsers);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        //implements the OnItemClickListener interface in the Adapter for when you click any item in the list
        adapter.setOnItemClickListener(new ManageUsersListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openItem(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected item
    public void openItem(DocumentSnapshot documentSnapshot, int position) {
        ManageUsersList manageUsersList = documentSnapshot.toObject(ManageUsersList.class);
        String id = documentSnapshot.getId(); //ID in FSDB
        String username = documentSnapshot.getString("displayName");
        String email = documentSnapshot.getString("emailAddress");
        String role = documentSnapshot.getString("role");
        //Start the activity and move the variables across
        Intent intent = new Intent(getActivity(), UserViewActivity.class);
        intent.putExtra(USER_ID, id);
        intent.putExtra(USER_NAME, username);
        intent.putExtra(USER_EMAIL, email);
        intent.putExtra(USER_ROLE, role);
        startActivity(intent);
    }

    //To tell the adapter to start and stop listening to database changes to avoid wasting resources when app is in background
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    //Toolbar and the buttons stuff:-----------------------------------------------------------------
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    //Linking the buttons in item_listings_menu.xml
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.manage_users_menu, menu);
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.firebaseAuthConsoleButton:
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                String userConsoleWebURL =
                        "https://console.firebase.google.com/u/0/project/nutbourne-inventory-manager/authentication/users";
                intent.putExtra(URL, userConsoleWebURL);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}