package com.example.nutbourneinventorymanagerapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private TextView userDisplayName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        getActivity().setTitle("Dashboard"); //change title of activity which changes name in toolbar
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        //Show welcome user with their display name at the top
        userDisplayName = view.findViewById(R.id.dashboard_header);//to link the header to the code
        if (user != null) {
            if (!user.getDisplayName().isEmpty()) {
                userDisplayName.setText("Welcome " + user.getDisplayName() + "!");
            } else {
                userDisplayName.setText("Welcome user !");
            }
        } else {
            userDisplayName.setText("No user name yet!");
        }

        hideButtons(view);

        //Buttons onclick ----initialise----------------
        Button itemButton = view.findViewById(R.id.items_button);
        Button assetButton = view.findViewById(R.id.assets_button);
        Button damagedItemsButton = view.findViewById(R.id.damaged_items_button);
        Button yourLogsButton = view.findViewById(R.id.your_logs_button);
        Button takenOutButton = view.findViewById(R.id.taken_out_button);
        Button requestedItemsButton = view.findViewById(R.id.requested_items_button);
        Button stockRequestsButton = view.findViewById(R.id.stock_requests_button);
        Button manageUsersButton = view.findViewById(R.id.manage_users_button);

        itemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ItemFragment()).addToBackStack(null).commit();
            }
        });
        assetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AssetFoldersFragment()).addToBackStack(null).commit();
            }
        });
        damagedItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DamagedItemsFragment()).addToBackStack(null).commit();
            }
        });
        yourLogsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new YourLogsFragment()).addToBackStack(null).commit();
            }
        });
        takenOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TakenOutFragment()).addToBackStack(null).commit();
            }
        });
        requestedItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RequestedItemsFragment()).addToBackStack(null).commit();
            }
        });
        stockRequestsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new StockRequestsFragment()).addToBackStack(null).commit();
            }
        });
        manageUsersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ManageUsersFragment()).addToBackStack(null).commit();
            }
        });
        return view;
    }

    //For hiding the dashboard buttons based on role.
    private void hideButtons(View view) {
        final Button stockRequestButton = view.findViewById(R.id.stock_requests_button);
        final Button manageUserButton = view.findViewById(R.id.manage_users_button);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference userRef = FirebaseFirestore.getInstance().collection("Users").document(user.getUid());
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        String roleUser = document.getString("role");
                        if (roleUser.equals("Administrator")) {
                            stockRequestButton.setVisibility(View.VISIBLE);
                            manageUserButton.setVisibility(View.VISIBLE);
                        } else {
                            if (roleUser.equals("Purchaser") || roleUser.equals("Manager")) {
                                stockRequestButton.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        //LEAVE EMPTY
    }
}