package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.AssetViewActivity;
import com.example.nutbourneinventorymanagerapp.Activities.NewAssetActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.AssetListing;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.AssetListingAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class AssetsFragment extends Fragment {

    //variables for passing the data to other activity
    public static final String ASSET_ID = "com.example.nutbourneinventorymanagerapp.ASSET_ID";
    public static final String ASSET_TITLE = "com.example.nutbourneinventorymanagerapp.ASSET_TITLE";
    public static final String ASSET_DESCRIPTION = "com.example.nutbourneinventorymanagerapp.ASSET_DESCRIPTION";
    public static final String ASSET_QUANTITY = "com.example.nutbourneinventorymanagerapp.ASSET_QUANTITY";
    public static final String ASSET_PRICE = "com.example.nutbourneinventorymanagerapp.ASSET_PRICE";
    public static final String ASSET_TAG = "com.example.nutbourneinventorymanagerapp.ASSET_TAG";
    public static final String ASSET_USER = "com.example.nutbourneinventorymanagerapp.ASSET_USER";
    public static final String ASSET_IMAGEURI = "com.example.nutbourneinventorymanagerapp.ASSET_IMAGEURI";
    public static final String CATEGORY_LOCATION = "com.example.nutbourneinventorymanagerapp.CATEGORY_LOCATION";
    private String subCollectionCategory;
    //Member variables to link the AssetListing recycler to the adapter
    private FirebaseFirestore db = FirebaseFirestore.getInstance(); //get connection to firestore
    private CollectionReference assetRef; //reference to the asset subcollection in firestore
    private AssetListingAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.assets_fragment, container, false);
        Log.d("log", "This is what subCollectionCategory is here in AssetsFragment: " + subCollectionCategory);
        assetRef = db.collection("Assets").document(subCollectionCategory).collection(subCollectionCategory);

        setUpRecyclerView(view); //passing view in here so I can use findViewById in this method
        getActivity().setTitle(subCollectionCategory); //change title of activity which changes name in toolbar

        return view;
    }

    //to link the recycler view to the adapter
    private void setUpRecyclerView(View view) {
        Query query = assetRef.orderBy("title", Query.Direction.ASCENDING); //orders by title
        //Getting the query into the adapter
        FirestoreRecyclerOptions<AssetListing> options = new FirestoreRecyclerOptions.Builder<AssetListing>()
                .setQuery(query, AssetListing.class).build();

        adapter = new AssetListingAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewAssets);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        //implements the OnItemClickListener interface in the Adapter
        adapter.setOnItemClickListener(new AssetListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                //What we want to do with the snapshot. I want to open new activity
                openAsset(documentSnapshot, position);
            }
        });
    }

    //method to move variables to AssetView activity and open it
    public void openAsset(DocumentSnapshot documentSnapshot, int position) {
        String id = documentSnapshot.getId(); //ID in FSDB
        String title = documentSnapshot.getString("title");
        String description = documentSnapshot.getString("description");
        Double quantity = documentSnapshot.getDouble("quantity");
        String price = documentSnapshot.getString("price");
        String tag = documentSnapshot.getString("assetTag");
        String assignedUser = documentSnapshot.getString("assignedUser");
        String imageUri = documentSnapshot.getString("imageData");
        //Start the activity and move the variables
        Intent intent = new Intent(getActivity(), AssetViewActivity.class);
        intent.putExtra(ASSET_ID, id);
        intent.putExtra(ASSET_TITLE, title);
        intent.putExtra(ASSET_DESCRIPTION, description);
        intent.putExtra(ASSET_QUANTITY, quantity);
        intent.putExtra(ASSET_PRICE, price);
        intent.putExtra(ASSET_TAG, tag);
        intent.putExtra(ASSET_USER, assignedUser);
        intent.putExtra(ASSET_IMAGEURI, imageUri);
        intent.putExtra(CATEGORY_LOCATION, subCollectionCategory);
        startActivity(intent);
    }

    public void updateSelectedCategory(CharSequence selectedCategory) { //for receiving and setting the selected category
        subCollectionCategory = selectedCategory.toString();
        Log.d("log3", "running updateSelectedCategory from AssetsFragment.java");
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }

    //These 3 methods pertain to the toolbar and their buttons.
    //For changing the toolbar buttons
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    //Giving the options menu for the toolbar
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.asset_listings_menu, menu);
        return;
    }

    //on click listener for the options menu in toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //item is used to check what was clicked
        switch (item.getItemId()) {
            case R.id.add_asset:
                openAddAsset(subCollectionCategory); //start the new asset activity
                return true;
            case R.id.option1:
                Toast.makeText(getActivity(), "Option 1 selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openAddAsset(String subCollectionCategory) {
        String categoryLocation = subCollectionCategory;
        Log.d("log7", "This is the category location to be sent to NewAssetActivity: " + categoryLocation);

        Intent intent = new Intent(getActivity(), NewAssetActivity.class);
        intent.putExtra(CATEGORY_LOCATION, categoryLocation); //pass the subcollection location currently selected
        startActivity(intent);
    }
}