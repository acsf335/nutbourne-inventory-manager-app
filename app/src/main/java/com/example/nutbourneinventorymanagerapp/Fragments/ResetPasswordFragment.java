package com.example.nutbourneinventorymanagerapp.Fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.nutbourneinventorymanagerapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

//Help for reset password came from Coding Demos video.
public class ResetPasswordFragment extends Fragment {

    EditText userEmail;
    Button confirmButton;
    ProgressBar progressBar;
    FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);

        progressBar = view.findViewById(R.id.progressBar);
        userEmail = view.findViewById(R.id.editTextConfirmEmail);
        confirmButton = view.findViewById(R.id.btnConfirm);

        mAuth = FirebaseAuth.getInstance();

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String email = userEmail.getText().toString().trim();
                String currentEmail = user.getEmail().trim();

                //Validation
                if (email.isEmpty()) {
                    userEmail.setError("Email is required!");
                    userEmail.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) { //if format is not email
                    userEmail.setError("Please enter a valid email");
                    userEmail.requestFocus();
                    return;
                }
                if (!email.equals(currentEmail)) { //if the entered email isn't same as logged in one
                    userEmail.setError("Incorrect Email.");
                    userEmail.requestFocus();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                //If pass validation, then:
                mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(),
                                    "Password reset email sent. Please check your email.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(),
                                    task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        getActivity().setTitle("Reset Password"); //change title of activity which changes name in toolbar
        return view;
    }
}