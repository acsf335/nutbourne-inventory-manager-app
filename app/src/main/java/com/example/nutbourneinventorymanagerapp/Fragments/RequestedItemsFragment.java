package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.ViewStockRequestActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.StockRequests;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.StockRequestsAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class RequestedItemsFragment extends Fragment {
    public static final String STOCKREQ_ID = "com.example.nutbourneinventorymanagerapp.STOCKREQ_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    //FireStore recycler stuff
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference stockRequestsRef = db.collection("Stock Requests"); //The link to the FSDB collection
    private StockRequestsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.requested_items_fragment, container, false);
        setUpRecyclerView(view);
        getActivity().setTitle("Requested Items"); //change title of activity which changes name in toolbar
        return view;
    }

    //FireStore Recycler stuff
    //Link the Item Recyclerview to the adapter
    private void setUpRecyclerView(View view) {
        String currentUid = FirebaseAuth.getInstance().getCurrentUser().getUid(); //current users id
        Query query = stockRequestsRef.whereEqualTo("userId", currentUid)
                .orderBy("dateRequired", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<StockRequests> options = new FirestoreRecyclerOptions.Builder<StockRequests>()
                .setQuery(query, StockRequests.class).build();

        adapter = new StockRequestsAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewRequestedItems);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new StockRequestsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openRequest(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected item
    public void openRequest(DocumentSnapshot documentSnapshot, int position) {
        String id = documentSnapshot.getId();
        Intent intent = new Intent(getActivity(), ViewStockRequestActivity.class);
        intent.putExtra(STOCKREQ_ID, id);
        startActivity(intent);
    }

    //To tell the adapter to start and stop listening to database changes to avoid wasting resources when app is in background
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}