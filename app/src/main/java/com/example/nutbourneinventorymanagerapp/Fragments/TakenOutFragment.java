package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.ViewLogActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.YourLogs;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.YourLogsAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class TakenOutFragment extends Fragment {

    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String LOG_ID = "com.example.nutbourneinventorymanagerapp.LOG_ID";
    public static final String PAGE_CHECK = "com.example.nutbourneinventorymanagerapp.PAGE_CHECK";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference yourLogsRef = db.collection("Your Logs");
    private YourLogsAdapter adapter;
    private String itemId; //id of the item of the log
    private String logId; //the logID field in this document

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.taken_out_fragment, container, false);
        getActivity().setTitle("Taken Out Items"); //change title of activity which changes name in toolbar
        setUpRecyclerView(view);
        return view;
    }

    private void setUpRecyclerView(View view) {
        String currentUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Query query = yourLogsRef.whereEqualTo("userID", currentUid).whereEqualTo("logType", "Taken Out").orderBy("timestamp", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<YourLogs> options = new FirestoreRecyclerOptions.Builder<YourLogs>()
                .setQuery(query, YourLogs.class).build();
        adapter = new YourLogsAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewTakenOutItems);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new YourLogsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openLog(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected item
    public void openLog(DocumentSnapshot documentSnapshot, int position) {
        String yourLogId = documentSnapshot.getId(); //id of log selected

        DocumentReference selectedLogRef = yourLogsRef.document(yourLogId);
        Log.d("Tag", "The id of the your log document selected is: " + yourLogId);

        selectedLogRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        itemId = document.getString("itemID");
                        logId = document.getString("logID");
                        Log.d("TAGS", "The item Id retrieved from this log is: " + itemId);
                        //Start the activity and move the variables.
                        Intent intent = new Intent(getActivity(), ViewLogActivity.class);
                        intent.putExtra(ITEM_ID, itemId);
                        intent.putExtra(LOG_ID, logId);
                        intent.putExtra(PAGE_CHECK, true);
                        Log.d("TAG", "Values to be sent from your logs fragment are: " + itemId + ", " + logId);
                        startActivity(intent);
                    } else {
                        Log.d("tag", "No such document");
                    }
                } else {
                    Log.d("Tag", "get failed with ", task.getException());
                }
            }
        });
    }

    //To tell the adapter when to start listening to database changes and when to stop
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() { //prevent the adapter from wasting resources if app is in background
        super.onStop();
        adapter.stopListening();
    }
}