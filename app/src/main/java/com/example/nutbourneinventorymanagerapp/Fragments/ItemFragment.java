package com.example.nutbourneinventorymanagerapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.Activities.ItemViewActivity;
import com.example.nutbourneinventorymanagerapp.Activities.NewItemActivity;
import com.example.nutbourneinventorymanagerapp.FireStoreRecycler.ItemListing;
import com.example.nutbourneinventorymanagerapp.FireStoreRecyclerAdapter.ItemListingAdapter;
import com.example.nutbourneinventorymanagerapp.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ItemFragment extends Fragment {
    //variables for passing the data to other activity
    public static final String ITEM_ID = "com.example.nutbourneinventorymanagerapp.ITEM_ID";
    public static final String ITEM_TITLE = "com.example.nutbourneinventorymanagerapp.ITEM_TITLE";
    public static final String ITEM_DESCRIPTION = "com.example.nutbourneinventorymanagerapp.ITEM_DESCRIPTION";
    public static final String ITEM_QUANTITY = "com.example.nutbourneinventorymanagerapp.ITEM_QUANTITY";
    public static final String ITEM_PRICE = "com.example.nutbourneinventorymanagerapp.ITEM_PRICE";
    public static final String ITEM_IMAGEURI = "com.example.nutbourneinventorymanagerapp.ITEM_IMAGEURI";
    //FireStore recycler stuff
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference itemRef = db.collection("Items"); //The link to the FSDB collection
    private ItemListingAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.items_fragment, container, false);
        setUpRecyclerView(view);
        getActivity().setTitle("Items"); //change title of activity which changes name in toolbar
        return view;
    }

    //FireStore Recycler stuff
    //Link the Item Recyclerview to the adapter
    private void setUpRecyclerView(View view) {
        Query query = itemRef.orderBy("title", Query.Direction.ASCENDING);
        //Getting the query into the adapter
        FirestoreRecyclerOptions<ItemListing> options = new FirestoreRecyclerOptions.Builder<ItemListing>()
                .setQuery(query, ItemListing.class).build();

        adapter = new ItemListingAdapter(options);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewItems);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        //implements the OnItemClickListener interface in the Adapter
        adapter.setOnItemClickListener(new ItemListingAdapter.OnItemClickListener() {
            @Override //what to do with that card's data
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                openItem(documentSnapshot, position);
            }
        });
    }

    //Method for opening the selected item
    public void openItem(DocumentSnapshot documentSnapshot, int position) {
        ItemListing itemListing = documentSnapshot.toObject(ItemListing.class);
        String id = documentSnapshot.getId(); //ID in FSDB
        String title = documentSnapshot.getString("title");
        String description = documentSnapshot.getString("description");
        Double quantity = documentSnapshot.getDouble("quantity");
        String price = documentSnapshot.getString("price");
        String imageUri = documentSnapshot.getString("imageData");
        //Start the activity and move the variables
        Intent intent = new Intent(getActivity(), ItemViewActivity.class);
        intent.putExtra(ITEM_ID, id);
        intent.putExtra(ITEM_TITLE, title);
        intent.putExtra(ITEM_DESCRIPTION, description);
        intent.putExtra(ITEM_QUANTITY, quantity);
        intent.putExtra(ITEM_PRICE, price);
        intent.putExtra(ITEM_IMAGEURI, imageUri);
        startActivity(intent);
    }

    //To tell the adapter to start and stop listening to database changes to avoid wasting resources when app is in background
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    //-----------------------------------------------------------------------------
    //Toolbar and the buttons stuff:-----------------------------------------------------------------
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    //Linking the buttons in item_listings_menu.xml
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.item_listings_menu, menu);
    }

    //OnClickListener for the buttons
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                startActivity(new Intent(getActivity(), NewItemActivity.class)); //start the new item activity
                return true;
            case R.id.option1: //will change later
                Toast.makeText(getActivity(), "Option 1 selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}