package com.example.nutbourneinventorymanagerapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutbourneinventorymanagerapp.R;
import com.example.nutbourneinventorymanagerapp.Recycler.SettingsRecycler;
import com.example.nutbourneinventorymanagerapp.RecyclerAdapter.SettingsRecyclerAdapter;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {

    private ArrayList<SettingsRecycler> mSettingsList;
    private RecyclerView mRecyclerView;
    private SettingsRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        createSettingsList();
        buildRecyclerView(view);
        getActivity().setTitle("Settings"); //change title of activity which changes name in toolbar
        return view;
    }

    public void createSettingsList() {
        mSettingsList = new ArrayList<>();
        mSettingsList.add(new SettingsRecycler(R.drawable.ic_manage_user, "Manage your account",
                "View and Edit your account details from here"));
    }

    private void buildRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerViewSettings);
        mRecyclerView.setHasFixedSize(true); //if recyclerview won't change in size regardless of items, then this increases performance.
        mLayoutManager = new LinearLayoutManager(getActivity()); //Coding in Flow said write "this" but this is a fragment!
        mAdapter = new SettingsRecyclerAdapter(mSettingsList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        //handle the click events. What pressing the button will do.
        mAdapter.setOnItemClickListener(new SettingsRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position == 0) { //starts from 0
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new ManageAccountFragment()).addToBackStack(null).commit();
                    mAdapter.notifyItemChanged(position);
                } else {
                    Toast.makeText(getActivity(), "Wrong position!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}